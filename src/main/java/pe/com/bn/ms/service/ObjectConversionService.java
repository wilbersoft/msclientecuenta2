package pe.com.bn.ms.service;

import pe.com.bn.ms.dto.GatewayRequest;

public interface ObjectConversionService
{
    // Convertir un objeto a trama
    <T> GatewayRequest convertDtoToPlot(T plotClass, String colophon, String operation, GatewayRequest response)
    throws Throwable;

    // Convertir una trama en objeto
    <T> T convertPlotToDto(T plotClass, String plot, String colophon) throws Throwable;

    // Validar headers de una trama, verificar si la transacción ha sido correcta
    <T> T validateHeadersForPlot(T plotClass, String plot, String colophon) throws Throwable;
}
