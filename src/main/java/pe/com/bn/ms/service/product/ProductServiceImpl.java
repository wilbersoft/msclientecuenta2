package pe.com.bn.ms.service.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.TypeSearchProduct;
import pe.com.bn.ms.domain.response.DataProduct.CustomProductResponse;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.dto.Product;
import pe.com.bn.ms.payload.GatewayClient;
import pe.com.bn.ms.service.ObjectConversionService;
import pe.com.bn.ms.util.Shared.DataUtil;
import pe.com.bn.ms.util.Shared.PropertiesParam;

import static pe.com.bn.ms.util.PayloadValidator.httpHeadersForGateway;
import static pe.com.bn.ms.util.PayloadValidator.validateGatewayResponse;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_CODE_TRANSACTION_612;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_COD_USER;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_LENGTH;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_TRANSACTION_PC01;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_READ;
import static pe.com.bn.ms.util.enums.TypeResult.RESULT_PRODUCT;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_CIC;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_DOCUMENT;
import static pe.com.bn.ms.util.functions.DateTimePicker.gatewayTimestamp;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.validatePlotResponse;

/**
 * @author prov_destrada
 * @created 22/10/2020 - 06:14 p. m.
 * @project ms-dataclients
 */
@Service
public class ProductServiceImpl implements ProductService
{
    private final ObjectConversionService objectConversionService;
    private final GatewayClient           gatewayClient;

    @Autowired
    public ProductServiceImpl(ObjectConversionService objectConversionService, GatewayClient gatewayClient)
    {
        this.objectConversionService = objectConversionService;
        this.gatewayClient           = gatewayClient;
    }

    /**
     * =================================================================================================================
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchProduct}
     * @return {@link Product}
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomProductResponse> findProductByTypeAndNumberDocument(HttpHeaders httpHeaders, TypeSearchProduct request)
    throws Throwable
    {
    	PropertiesParam props = new PropertiesParam();
    	
    	// Parametro del HEADER
    	String hdLong = DataUtil.formatearCampo(props.getHdLong(), 4, "C", true,1,5);
        String hdTran = DataUtil.formatearCampo(props.getHdTran(), 4, "C", true,5,9);
        String hdCodTran1Format = DataUtil.formatearCampo(props.getHdCodTran1Format(), 4, "N", true,35,39);
        String hdTimestamp = DataUtil.formatearCampo(props.getHdTimestamp(), 26, "C", false,9,35);
        String hdCodUser = DataUtil.formatearCampo(request.getUserApplication(), 6, "C", true,39,45);
        String hdDesRestFormat = DataUtil.formatearCampo(0, 5, "N", false,45, 50);
        String hdCodCanal = DataUtil.formatearCampo("", 4, "C", false,50,79);
        String hdCodTerm = DataUtil.formatearCampo("", 6, "C", false,79,85);
        String hdCodRetLog = DataUtil.formatearCampo("", 5, "C", false,85,90);
        String hdDesRetLog = DataUtil.formatearCampo("", 22, "C", false,90,112);
	
    	//Parametros del BODY INPUT

        String inTdcmtoId = DataUtil.formatearCampo("", 1, "C", false,112,113);
        String inNdcmtoIdFormat = DataUtil.formatearCampo(request.getNumberDocument(), 12, "N", true,113,125);
        String inTipoBusFormat = DataUtil.formatearCampo(1, 2, "N", true,125,127);
        String inTipoResultFormat = DataUtil.formatearCampo(1, 2, "N", true,127,129);
        String inUltctaFormat = DataUtil.formatearCampo(0, 4, "N", false,129,133);
        String inCicFormat = DataUtil.formatearCampo(request.getCic(), 12, "N", true,133,145);
        String inCodProdFormat = DataUtil.formatearCampo(request.getTypeProduct(), 2, "N", false,145,147);
        String inNumProdFormat = DataUtil.formatearCampo(request.getNumProduct(), 18, "N", false,147,165);
        String inNumCorDirFormat = DataUtil.formatearCampo(0, 5, "N", false,165,170);
        String inNtarjetaInputFormat = DataUtil.formatearCampo(0, 16, "N", false,170,186);
        String inTdocumAdFormat = DataUtil.formatearCampo(request.getTypeDocument(), 2, "N", false,186,188);
        String inFiller = DataUtil.formatearCampo("", 24, "C", false,188,212);
        String HD_DES_RET = DataUtil.formatearCampo(0, 25, "N", false,50,75);
        
        //Parametros de entrada de la API
        //request.setTypeSearch(inTipoBusFormat);
        request.setCic(inCicFormat);
        request.setTypeDocument(inTdocumAdFormat);
        request.setNumberDocument(inNdcmtoIdFormat);
        request.setTypeProduct(inCodProdFormat);
        request.setNumProduct(inNumProdFormat);
        request.setUserApplication(hdCodUser);
        
        
        // Construir la cadena con los campos formateados
        StringBuilder tramaFormateada = new StringBuilder();
        tramaFormateada.append(hdLong).append(hdTran).append(hdTimestamp).append(hdCodTran1Format).
        append(hdCodUser).append(hdDesRestFormat).append(HD_DES_RET).append(hdCodCanal).append(hdCodTerm).append(hdCodRetLog).
        append(hdDesRetLog).append(inTdcmtoId).append(inNdcmtoIdFormat).append(inTipoBusFormat).append(inTipoResultFormat).
        append(inUltctaFormat).append(inCicFormat).append(inCodProdFormat).append(inNumProdFormat).append(inNumCorDirFormat).
        append(inNtarjetaInputFormat).append(inTdocumAdFormat).append(inFiller);
        
        System.out.print("TRAMA SEARCH NUM DOC FORMATEADA (PRODUCTOS): "+tramaFormateada);
        
        return new GenericResponse<CustomProductResponse>();
    	
    }

    /**
     * =================================================================================================================
     * Buscar producto por código único de cliente
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchProduct}
     * @return {@link Product}
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomProductResponse> findProductByCic(HttpHeaders httpHeaders, TypeSearchProduct request) throws Throwable
    {

    	PropertiesParam props = new PropertiesParam();
    	
    	// Parametro del HEADER
    	String hdLong = DataUtil.formatearCampo(props.getHdLong(), 4, "C", true,1,5);
        String hdTran = DataUtil.formatearCampo(props.getHdTran(), 4, "C", true,5,9);
        String hdCodTran1Format = DataUtil.formatearCampo(props.getHdCodTran1Format(), 4, "N", true,35,39);
        String hdTimestamp = DataUtil.formatearCampo(props.getHdTimestamp(), 26, "C", false,9,35);
        String hdCodUser = DataUtil.formatearCampo(request.getUserApplication(), 6, "C", true,39,45);
        String hdDesRestFormat = DataUtil.formatearCampo(0, 5, "N", false,45, 50);
        String hdCodCanal = DataUtil.formatearCampo("", 4, "C", false,50,79);
        String hdCodTerm = DataUtil.formatearCampo("", 6, "C", false,79,85);
        String hdCodRetLog = DataUtil.formatearCampo("", 5, "C", false,85,90);
        String hdDesRetLog = DataUtil.formatearCampo("", 22, "C", false,90,112);
	
    	//Parametros del BODY INPUT

        String inTdcmtoId = DataUtil.formatearCampo("", 1, "C", false,112,113);
        String inNdcmtoIdFormat = DataUtil.formatearCampo(request.getNumberDocument(), 12, "N", true,113,125);
        String inTipoBusFormat = DataUtil.formatearCampo(2, 2, "N", true,125,127);
        String inTipoResultFormat = DataUtil.formatearCampo(1, 2, "N", true,127,129);
        String inUltctaFormat = DataUtil.formatearCampo(0, 4, "N", false,129,133);
        String inCicFormat = DataUtil.formatearCampo(request.getCic(), 12, "N", true,133,145);
        String inCodProdFormat = DataUtil.formatearCampo(request.getTypeProduct(), 2, "N", false,145,147);
        String inNumProdFormat = DataUtil.formatearCampo(request.getNumProduct(), 18, "N", false,147,165);
        String inNumCorDirFormat = DataUtil.formatearCampo(0, 5, "N", false,165,170);
        String inNtarjetaInputFormat = DataUtil.formatearCampo(0, 16, "N", false,170,186);
        String inTdocumAdFormat = DataUtil.formatearCampo(request.getTypeDocument(), 2, "N", false,186,188);
        String inFiller = DataUtil.formatearCampo("", 24, "C", false,188,212);
        String HD_DES_RET = DataUtil.formatearCampo(0, 25, "N", false,50,75);
        
        //Parametros de entrada de la API
        //request.setTypeSearch(inTipoBusFormat);
        request.setCic(inCicFormat);
        request.setTypeDocument(inTdocumAdFormat);
        request.setNumberDocument(inNdcmtoIdFormat);
        request.setTypeProduct(inCodProdFormat);
        request.setNumProduct(inNumProdFormat);
        request.setUserApplication(hdCodUser);
        
        
        // Construir la cadena con los campos formateados
        StringBuilder tramaFormateada = new StringBuilder();
        tramaFormateada.append(hdLong).append(hdTran).append(hdTimestamp).append(hdCodTran1Format).
        append(hdCodUser).append(hdDesRestFormat).append(HD_DES_RET).append(hdCodCanal).append(hdCodTerm).append(hdCodRetLog).
        append(hdDesRetLog).append(inTdcmtoId).append(inNdcmtoIdFormat).append(inTipoBusFormat).append(inTipoResultFormat).
        append(inUltctaFormat).append(inCicFormat).append(inCodProdFormat).append(inNumProdFormat).append(inNumCorDirFormat).
        append(inNtarjetaInputFormat).append(inTdocumAdFormat).append(inFiller);
        
        System.out.print("TRAMA SEARCH CIC FORMATEADA (PRODUCTOS): "+tramaFormateada);
    	
        return new GenericResponse<CustomProductResponse>();
    }

	@Override
	public GenericResponse<CustomProductResponse> findCustomerByTypeAndNumberProduct(HttpHeaders httpHeaders, TypeSearchProduct request)
			throws Throwable {
		
		PropertiesParam props = new PropertiesParam();
		
		// Parametro del HEADER
    	String hdLong = DataUtil.formatearCampo(props.getHdLong(), 4, "C", true,1,5);
        String hdTran = DataUtil.formatearCampo(props.getHdTran(), 4, "C", true,5,9);
        String hdCodTran1Format = DataUtil.formatearCampo(props.getHdCodTran1Format(), 4, "N", true,35,39);
        String hdTimestamp = DataUtil.formatearCampo(props.getHdTimestamp(), 26, "C", false,9,35);
        String hdCodUser = DataUtil.formatearCampo(request.getUserApplication(), 6, "C", true,39,45);
        String hdDesRestFormat = DataUtil.formatearCampo(0, 5, "N", false,45, 50);
        String hdCodCanal = DataUtil.formatearCampo("", 4, "C", false,50,79);
        String hdCodTerm = DataUtil.formatearCampo("", 6, "C", false,79,85);
        String hdCodRetLog = DataUtil.formatearCampo("", 5, "C", false,85,90);
        String hdDesRetLog = DataUtil.formatearCampo("", 22, "C", false,90,112);
	
    	//Parametros del BODY INPUT

        String inTdcmtoId = DataUtil.formatearCampo("", 1, "C", false,112,113);
        String inNdcmtoIdFormat = DataUtil.formatearCampo(request.getNumberDocument(), 12, "N", true,113,125);
        String inTipoBusFormat = DataUtil.formatearCampo(3, 2, "N", true,125,127);
        String inTipoResultFormat = DataUtil.formatearCampo(1, 2, "N", true,127,129);
        String inUltctaFormat = DataUtil.formatearCampo(0, 4, "N", false,129,133);
        String inCicFormat = DataUtil.formatearCampo(request.getCic(), 12, "N", true,133,145);
        String inCodProdFormat = DataUtil.formatearCampo(request.getTypeProduct(), 2, "N", false,145,147);
        String inNumProdFormat = DataUtil.formatearCampo(request.getNumProduct(), 18, "N", false,147,165);
        String inNumCorDirFormat = DataUtil.formatearCampo(0, 5, "N", false,165,170);
        String inNtarjetaInputFormat = DataUtil.formatearCampo(0, 16, "N", false,170,186);
        String inTdocumAdFormat = DataUtil.formatearCampo(request.getTypeDocument(), 2, "N", false,186,188);
        String inFiller = DataUtil.formatearCampo("", 24, "C", false,188,212);
        String HD_DES_RET = DataUtil.formatearCampo(0, 25, "N", false,50,75);
        
        //Parametros de entrada de la API
        //request.setTypeSearch(inTipoBusFormat);
        request.setCic(inCicFormat);
        request.setTypeDocument(inTdocumAdFormat);
        request.setNumberDocument(inNdcmtoIdFormat);
        request.setTypeProduct(inCodProdFormat);
        request.setNumProduct(inNumProdFormat);
        request.setUserApplication(hdCodUser);
        
        
        // Construir la cadena con los campos formateados
        StringBuilder tramaFormateada = new StringBuilder();
        tramaFormateada.append(hdLong).append(hdTran).append(hdTimestamp).append(hdCodTran1Format).
        append(hdCodUser).append(hdDesRestFormat).append(HD_DES_RET).append(hdCodCanal).append(hdCodTerm).append(hdCodRetLog).
        append(hdDesRetLog).append(inTdcmtoId).append(inNdcmtoIdFormat).append(inTipoBusFormat).append(inTipoResultFormat).
        append(inUltctaFormat).append(inCicFormat).append(inCodProdFormat).append(inNumProdFormat).append(inNumCorDirFormat).
        append(inNtarjetaInputFormat).append(inTdocumAdFormat).append(inFiller);
        
        System.out.print("TRAMA SEARCH NUM PRODUCT FORMATEADA (PRODUCTOS): "+tramaFormateada);
    			
		return new GenericResponse<CustomProductResponse>();
	}
}
