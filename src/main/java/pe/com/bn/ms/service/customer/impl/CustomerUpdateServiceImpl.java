package pe.com.bn.ms.service.customer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.dto.PersonaCliente;
import pe.com.bn.ms.payload.GatewayClient;
import pe.com.bn.ms.service.ObjectConversionService;
import pe.com.bn.ms.service.customer.CustomerUpdateService;

import static pe.com.bn.ms.util.PayloadValidator.httpHeadersForGateway;
import static pe.com.bn.ms.util.PayloadValidator.validateGatewayResponse;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_CODE_TRANSACTION_WS00;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_LENGTH;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_UPDATE;

/**
 * @author prov_destrada
 * @created 21/10/2020 - 05:13 p. m.
 * @project ms-dataclients
 */
@Service
public class CustomerUpdateServiceImpl implements CustomerUpdateService
{
    private final ObjectConversionService objectConversionService;
    private final GatewayClient           gatewayClient;

    @Autowired
    public CustomerUpdateServiceImpl(ObjectConversionService objectConversionService,
                                     GatewayClient gatewayClient)
    {
        this.objectConversionService = objectConversionService;
        this.gatewayClient           = gatewayClient;
    }

    /**
     * =================================================================================================================
     * Actualizar datos del cliente
     *
     * @param personaCliente {@link PersonaCliente}
     * @param httpHeaders    cabeceras de la aplicación
     * @return
     * @throws Throwable error
     */
    @Override
    public GenericResponse<?> updateClientByCIC(PersonaCliente personaCliente, HttpHeaders httpHeaders) throws Throwable
    {
        personaCliente.setHdLong(HEADER_LENGTH.getValue());
        personaCliente.setHdTran(HEADER_CODE_TRANSACTION_WS00.getValue());

        // ----------------------------------------------
        // Generar el request body gateway
        GatewayRequest gwRequest = new GatewayRequest();
        gwRequest.setLongitud(personaCliente.getHdLong());
        gwRequest.setTransId(personaCliente.getHdTran());

        gwRequest = objectConversionService
                .convertDtoToPlot(personaCliente, "", OPERATION_UPDATE.getValue(), gwRequest);

        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient
                .runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);

        // Validar la respuesta
        validateGatewayResponse(gwResponseEnt);

        return new GenericResponse<>("OK");
    }

}
