package pe.com.bn.ms.service.address;

import org.springframework.stereotype.Service;
import pe.com.bn.ms.dto.Direccion;
import pe.com.bn.ms.dto.PersonaCliente;

@Service
public class AddressServiceImpl implements AddressService
{

    @Override
    public Direccion findAddressByCic(String CIC) throws Exception
    {
        return null;

    }

    @Override
    public Direccion findAddressByCicAndCorrelative(String CIC, String numCorre) throws Exception
    {

        return null;
    }

    @Override
    public Direccion createClientAddress(PersonaCliente personaCliente) throws Exception
    {

        return null;
    }

    @Override
    public Direccion updateClientAddress(PersonaCliente personaCliente) throws Exception
    {

        return null;
    }
}
