package pe.com.bn.ms.service.customer.impl;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.ClienteContactabilidad;
import pe.com.bn.ms.dto.Customer;
import pe.com.bn.ms.dto.CustomerAccount;
import pe.com.bn.ms.dto.CustomerAccountData;
import pe.com.bn.ms.dto.CustomerAccountDetail;
import pe.com.bn.ms.dto.CustomerAccountSIDGData;
import pe.com.bn.ms.dto.CustomerAddress;
import pe.com.bn.ms.dto.CustomerCtaSIDG;
import pe.com.bn.ms.dto.CustomerSIDG;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.payload.GatewayClient;
import pe.com.bn.ms.service.ObjectConversionService;
import pe.com.bn.ms.service.customer.CustomerCreateService;

import java.text.DecimalFormat;
import java.time.LocalDate;

import static pe.com.bn.ms.util.PayloadValidator.httpHeadersForGateway;
import static pe.com.bn.ms.util.PayloadValidator.validateGatewayResponse;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_LENGTH_SIUC;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_USER;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_CREATE;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.validatePlotResponse;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.validatePlotResponse2;

/**
 * @author prov_destrada
 * @created 21/10/2020 - 05:12 p. m.
 * @project ms-dataclients
 */
@Service
public class CustomerCreateServiceImpl implements CustomerCreateService
{
    private final ObjectConversionService objectConversionService;
    private final GatewayClient           gatewayClient;

    public CustomerCreateServiceImpl(ObjectConversionService objectConversionService,
                                     GatewayClient gatewayClient)
    {
        this.objectConversionService = objectConversionService;
        this.gatewayClient           = gatewayClient;
    }

    /**
     * =================================================================================================================
     * Registrar nuevo cliente
     *
     * @param request     {@link Customer}
     * @param httpHeaders cabeceras de aplicación
     * @return
     * @throws Throwable error
     */
    @Override
    public GenericResponse<?> createNewCustomer(Customer request, HttpHeaders httpHeaders)
    throws Throwable
    {
        request.setHdLong(HEADER_LENGTH_SIUC.getValue());
        request.setHdTransaction(HEADER_LENGTH_SIUC.getValue());
        request.setHdUser(HEADER_USER.getValue());

        LocalDate now = LocalDate.now();
        
        DecimalFormat mFormat= new DecimalFormat("00");
        
        String mes =  mFormat.format(Double.valueOf(now.getMonthValue()));
        String dia =  mFormat.format(Double.valueOf(now.getDayOfMonth()));
        

        request.setHdYear(String.valueOf(now.getYear()));
        request.setHdMonth(mes);
        request.setHdDay(dia);

        request.setHdSequential("98");
        request.setHdRequest("P");
        request.setHdForma("I");
        request.setHdType("CL");
        request.setHdProgram("SIUC9013");

        // 6 - PJ, otros PN
        //request.setHdFill("PN");
        String tipoPersona = "PN";
        if(request.getTypeDocument().trim().equalsIgnoreCase("6")) {
        	tipoPersona = "PJ";
        }
        request.setHdModification(tipoPersona);

        request.getCustomerAccountDetail().setTypeEventAccount("51");
        
        //seteo datacliente substring
        request.setCustomerCic(validateStringLength(request.getCustomerCic(),12));
        request.setInputCodeOffice(validateStringLength(request.getInputCodeOffice(),4));
        request.setOperationalCodeOffice(validateStringLength(request.getOperationalCodeOffice(), 4));
        request.setTypeDocument(validateStringLength(request.getTypeDocument(), 1));
        request.setNumberDocument(validateStringLength(request.getNumberDocument(), 12));
        request.setFlagUnderAge(validateStringLength(request.getFlagUnderAge(), 1));
        request.setFlagInterdicto(validateStringLength(request.getFlagInterdicto(), 1));
        request.setLastName(validateStringLength(request.getLastName(), 50));
        request.setMotherLastName(validateStringLength(request.getMotherLastName(), 50));
        request.setMarriedLastName(validateStringLength(request.getMarriedLastName(), 50));
        request.setFirstName(validateStringLength(request.getFirstName(), 50));
        request.setSecondName(validateStringLength(request.getSecondName(), 50));
        request.setCodeGender(validateStringLength(request.getCodeGender(), 1));
        request.setCodeCivilStatus(validateStringLength(request.getCodeCivilStatus(), 1));
        request.setYearsOfStudy(validateStringLength(request.getYearsOfStudy(), 1));
        request.setCustomerHeight(validateStringLength(request.getCustomerHeight(), 3));
        request.setFathersName(validateStringLength(request.getFathersName(), 15));
        request.setMothersName(validateStringLength(request.getMothersName(), 15));
        request.setCodeScaleOfInstruction(validateStringLength(request.getCodeScaleOfInstruction(), 1));
        request.setBirthdate(validateStringLength(request.getBirthdate(), 8));
        request.setCodeInternalCustomer(validateStringLength(request.getCodeInternalCustomer(), 6));
        request.setCodeInternalGroup(validateStringLength(request.getCodeInternalGroup(), 3));
        request.setStatusRelationship(validateStringLength(request.getStatusRelationship(), 1));
        request.setNumberPhone(validateStringLength(request.getNumberPhone(), 12));
        request.setCodeCustomerEmployment(validateStringLength(request.getCodeCustomerEmployment(), 5));
        request.setCodeEntityCustomer(validateStringLength(request.getCodeEntityCustomer(), 6));
        request.setCustomerFill(validateStringLength(request.getCustomerFill(), 16));
        //seteo dataclientedireccion substring
        CustomerAddress customerAddress = request.getCustomerAddress();
        customerAddress.setTypeRoad(validateStringLength(customerAddress.getTypeRoad(),1));
        customerAddress.setTypeAddress(validateStringLength(customerAddress.getTypeAddress(),20));
        customerAddress.setRoadDescription(validateStringLength(customerAddress.getRoadDescription(),60));
        customerAddress.setAddressNumber(validateStringLength(customerAddress.getAddressNumber(),30));
        customerAddress.setBlockNumber(validateStringLength(customerAddress.getBlockNumber(),30));
        customerAddress.setSquareNumber(validateStringLength(customerAddress.getSquareNumber(),30));
        customerAddress.setNumberLot(validateStringLength(customerAddress.getNumberLot(),30));
        customerAddress.setNumberFlat(validateStringLength(customerAddress.getNumberFlat(),30));
        customerAddress.setNumberDepartment(validateStringLength(customerAddress.getNumberDepartment(),30));
        customerAddress.setAddressReference(validateStringLength(customerAddress.getAddressReference(),45));
        customerAddress.setAddressFill(validateStringLength(customerAddress.getAddressFill(),5));
        request.setCustomerAddress(customerAddress);
        //seteo dataclientecuenta substring
        CustomerAccount customerAccount = request.getCustomerAccount();
        customerAccount.setCodeProduct(validateStringLength(customerAccount.getCodeProduct(), 2));
        customerAccount.setAccountNumber(validateStringLength(customerAccount.getAccountNumber(), 18));
        customerAccount.setTypeMoney(validateStringLength(customerAccount.getTypeMoney(), 3));
        customerAccount.setAccountShortName(validateStringLength(customerAccount.getAccountShortName(), 40));
        customerAccount.setAccountName(validateStringLength(customerAccount.getAccountName(), 150));
        customerAccount.setItemAddressDom(validateStringLength(customerAccount.getItemAddressDom(), 5));
        customerAccount.setItemAddressCore(validateStringLength(customerAccount.getItemAddressCore(), 5));
        customerAccount.setItemAddressWorkCenter(validateStringLength(customerAccount.getItemAddressWorkCenter(), 5));
        customerAccount.setItemAddressFiscal(validateStringLength(customerAccount.getItemAddressFiscal(), 5));
        customerAccount.setAccountSituationCode(validateStringLength(customerAccount.getAccountSituationCode(), 2));
        customerAccount.setTypeAccount(validateStringLength(customerAccount.getTypeAccount(), 2));
        customerAccount.setFlagAccountUOB(validateStringLength(customerAccount.getFlagAccountUOB(), 1));
        customerAccount.setTypeUbigee(validateStringLength(customerAccount.getTypeUbigee(), 1));
        customerAccount.setCodeUbigee(validateStringLength(customerAccount.getCodeUbigee(), 6));
        customerAccount.setCustomerAccountFill(validateStringLength(customerAccount.getCustomerAccountFill(), 25));
        customerAccount.setShippingMeansCode(validateStringLength(customerAccount.getShippingMeansCode(), 2));
        customerAccount.setTypeEvent(validateStringLength(customerAccount.getTypeEvent(), 2));
        customerAccount.setReasonEvent(validateStringLength(customerAccount.getReasonEvent(), 1));
        customerAccount.setCreationReasonCode(validateStringLength(customerAccount.getCreationReasonCode(), 3));
        customerAccount.setFlagBlacklist(validateStringLength(customerAccount.getFlagBlacklist(), 1));
        customerAccount.setFirstAgreementDate(validateStringLength(customerAccount.getFirstAgreementDate(), 8));
        request.setCustomerAccount(customerAccount);
        //seteo dataclientecuentadetail substring
        CustomerAccountDetail customerAccountDetail = request.getCustomerAccountDetail();
        customerAccountDetail.setTypeEventAccount(validateStringLength(customerAccountDetail.getTypeEventAccount(),2));
        customerAccountDetail.setBusinessExecutiveCode(validateStringLength(customerAccountDetail.getBusinessExecutiveCode(),6));
        customerAccountDetail.setTypePerson(validateStringLength(customerAccountDetail.getTypePerson(),1));
        customerAccountDetail.setTypeDocumentReport(validateStringLength(customerAccountDetail.getTypeDocumentReport(),2));
        customerAccountDetail.setNumberDocumentReport(validateStringLength(customerAccountDetail.getNumberDocumentReport(),12));
        customerAccountDetail.setFullNameReport(validateStringLength(customerAccountDetail.getFullNameReport(),150));
        customerAccountDetail.setTypeCustomer(validateStringLength(customerAccountDetail.getTypeCustomer(),2));
        customerAccountDetail.setFlagCountryResidence(validateStringLength(customerAccountDetail.getFlagCountryResidence(),1));
        customerAccountDetail.setFlagNationality(validateStringLength(customerAccountDetail.getFlagNationality(),1));
        customerAccountDetail.setCodeCountryOfResidence(validateStringLength(customerAccountDetail.getCodeCountryOfResidence(),4));
        customerAccountDetail.setCodeRelationshipWorkingBco(validateStringLength(customerAccountDetail.getCodeRelationshipWorkingBco(),1));
        customerAccountDetail.setCodeShareholderBco(validateStringLength(customerAccountDetail.getCodeShareholderBco(),1));
        customerAccountDetail.setCodeEconomicSector(validateStringLength(customerAccountDetail.getCodeEconomicSector(),6));
        customerAccountDetail.setCodeEconomicActivity(validateStringLength(customerAccountDetail.getCodeEconomicActivity(),6));
        customerAccountDetail.setCodeDebtorClass(validateStringLength(customerAccountDetail.getCodeDebtorClass(),1));
        customerAccountDetail.setCodeRisk(validateStringLength(customerAccountDetail.getCodeRisk(),1));
        customerAccountDetail.setCustomerMagnitudeCode(validateStringLength(customerAccountDetail.getCustomerMagnitudeCode(),1));
        customerAccountDetail.setDescriptionDebtorClassSBS(validateStringLength(customerAccountDetail.getDescriptionDebtorClassSBS(),10));
        
        customerAccountDetail.setAnnualProfit(validateStringLength(customerAccountDetail.getAnnualProfit(),15));
        
        customerAccountDetail.setNumberRUC(validateStringLength(customerAccountDetail.getNumberRUC(),12));
        customerAccountDetail.setCompanyName(validateStringLength(customerAccountDetail.getCompanyName(),150));
        customerAccountDetail.setCompanyAcronym(validateStringLength(customerAccountDetail.getCompanyAcronym(),60));
        customerAccountDetail.setCodeOfficeRegistry(validateStringLength(customerAccountDetail.getCodeOfficeRegistry(),2));
        customerAccountDetail.setSubCodeOfficeRegistry(validateStringLength(customerAccountDetail.getSubCodeOfficeRegistry(),2));
        customerAccountDetail.setCompanyCreationDate(validateStringLength(customerAccountDetail.getCompanyCreationDate(),8));
        customerAccountDetail.setRegistryPublicID(validateStringLength(customerAccountDetail.getRegistryPublicID(),1));
        customerAccountDetail.setRegistryPublicNumberCertificate(validateStringLength(customerAccountDetail.getRegistryPublicNumberCertificate(),10));
        customerAccountDetail.setRegistryPublicNumberFolio(validateStringLength(customerAccountDetail.getRegistryPublicNumberFolio(),3));
        customerAccountDetail.setRegistryPublicNumberVolume(validateStringLength(customerAccountDetail.getRegistryPublicNumberVolume(),7));
        customerAccountDetail.setRegistryPublicReported(validateStringLength(customerAccountDetail.getRegistryPublicReported(),20));
        customerAccountDetail.setReferenceDocument(validateStringLength(customerAccountDetail.getReferenceDocument(),30));
        customerAccountDetail.setTdCmToAh(validateStringLength(customerAccountDetail.getTdCmToAh(),1));
        customerAccountDetail.setCodeDegreeEducationAccount(validateStringLength(customerAccountDetail.getCodeDegreeEducationAccount(),12));
        customerAccountDetail.setCodeProfessionAccount(validateStringLength(customerAccountDetail.getCodeProfessionAccount(),1));
        customerAccountDetail.setCodeDelayAccount(validateStringLength(customerAccountDetail.getCodeDelayAccount(),1));
        customerAccountDetail.setCodeDebtorClassification(validateStringLength(customerAccountDetail.getCodeDebtorClassification(),1));
        customerAccountDetail.setCodeDebtorClassificationAlign(validateStringLength(customerAccountDetail.getCodeDebtorClassificationAlign(),1));
        customerAccountDetail.setTypeDocumentComplementary(validateStringLength(customerAccountDetail.getTypeDocumentComplementary(),1));
        customerAccountDetail.setNumberDocumentComplementary(validateStringLength(customerAccountDetail.getNumberDocumentComplementary(),12));
        customerAccountDetail.setFlagPersonWithBusiness(validateStringLength(customerAccountDetail.getFlagPersonWithBusiness(),1));
        customerAccountDetail.setCodeCountryOfBirth(validateStringLength(customerAccountDetail.getCodeCountryOfBirth(),4));
        customerAccountDetail.setFlagDeceased(validateStringLength(customerAccountDetail.getFlagDeceased(),1));
        customerAccountDetail.setCodeDebtorClassSBS(validateStringLength(customerAccountDetail.getCodeDebtorClassSBS(),1));
        customerAccountDetail.setTypeOfBank(validateStringLength(customerAccountDetail.getTypeOfBank(),2));
        customerAccountDetail.setSegmentClientCode(validateStringLength(customerAccountDetail.getSegmentClientCode(),2));
        customerAccountDetail.setCodePublicSector(validateStringLength(customerAccountDetail.getCodePublicSector(),3));
        customerAccountDetail.setNumberDependents(validateStringLength(customerAccountDetail.getNumberDependents(),2));
        customerAccountDetail.setCodeEmployment(validateStringLength(customerAccountDetail.getCodeEmployment(),5));
        customerAccountDetail.setFlagVisitHome(validateStringLength(customerAccountDetail.getFlagVisitHome(),1));
        customerAccountDetail.setAccountHomePhone(validateStringLength(customerAccountDetail.getAccountHomePhone(),12));
        customerAccountDetail.setAccountWorkingPhone(validateStringLength(customerAccountDetail.getAccountWorkingPhone(),12));
        customerAccountDetail.setAccountWorkingPhoneAnnex(validateStringLength(customerAccountDetail.getAccountWorkingPhoneAnnex(),6));
        
        //customerAccountDetail.setCellphone(validateStringLength(customerAccountDetail.getCellphone(),12));
        customerAccountDetail.setCellphone(customerAccountDetail.getCellphone());
        
        customerAccountDetail.setPersonalEmail(validateStringLength(customerAccountDetail.getPersonalEmail(),96));
        customerAccountDetail.setOptionalEmail(validateStringLength(customerAccountDetail.getOptionalEmail(),96));
        customerAccountDetail.setWebPage(validateStringLength(customerAccountDetail.getWebPage(),96));
        customerAccountDetail.setRegPubReportado02(validateStringLength(customerAccountDetail.getRegPubReportado02(),1));
        customerAccountDetail.setGPersonExpPub(validateStringLength(customerAccountDetail.getGPersonExpPub(),1));
        customerAccountDetail.setReported(validateStringLength(customerAccountDetail.getReported(),150));
        customerAccountDetail.setTUbigeeBirth(validateStringLength(customerAccountDetail.getTUbigeeBirth(),1));
        customerAccountDetail.setCUbigeeBirth(validateStringLength(customerAccountDetail.getCUbigeeBirth(),8));
        customerAccountDetail.setAccountRelated(validateStringLength(customerAccountDetail.getAccountRelated(),2));
        customerAccountDetail.setAccountCustomerCic(validateStringLength(customerAccountDetail.getAccountCustomerCic(),12));
        customerAccountDetail.setRelation(validateStringLength(customerAccountDetail.getRelation(),1));
        customerAccountDetail.setStartRelationship(validateStringLength(customerAccountDetail.getStartRelationship(),8));
        customerAccountDetail.setEndRelationship(validateStringLength(customerAccountDetail.getEndRelationship(),8));
        customerAccountDetail.setPorcPartC(validateStringLength(customerAccountDetail.getPorcPartC(),3));
        customerAccountDetail.setDocumented(validateStringLength(customerAccountDetail.getDocumented(),1));
        customerAccountDetail.setRelatedObservations(validateStringLength(customerAccountDetail.getRelatedObservations(),30));
        customerAccountDetail.setIndicatorBIN(validateStringLength(customerAccountDetail.getIndicatorBIN(),1));
        customerAccountDetail.setTUbigeeSavingsAccount(validateStringLength(customerAccountDetail.getTUbigeeSavingsAccount(),1));
        customerAccountDetail.setCUbigeeSavingsAccount(validateStringLength(customerAccountDetail.getCUbigeeSavingsAccount(),8));
        customerAccountDetail.setTUbigeeCustomerAccount(validateStringLength(customerAccountDetail.getTUbigeeCustomerAccount(),1));
        customerAccountDetail.setCUbigeeCustomerAccount(validateStringLength(customerAccountDetail.getCUbigeeCustomerAccount(),8));
        customerAccountDetail.setAccountFill(validateStringLength(customerAccountDetail.getAccountFill(),729));
        request.setCustomerAccountDetail(customerAccountDetail);

        // ----------------------------------------------
        // Generar el request body gateway
        GatewayRequest gwRequest = new GatewayRequest();
        gwRequest.setLongitud(request.getHdLong());
        gwRequest.setTransId(request.getHdTransaction());

        gwRequest = objectConversionService.convertDtoToPlot(request, "", OPERATION_CREATE.getValue(), gwRequest);

        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient.runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);

        // -----------------------------------------
        // Validar la respuesta
        validateGatewayResponse(gwResponseEnt);
        Customer customerResponse = objectConversionService
                .validateHeadersForPlot(new Customer(), gwResponseEnt.getBody().getDatos(), "");

        validatePlotResponse2(customerResponse);
        
        String trama = gwResponseEnt.getBody().getDatos();
        String codigoCIC = trama.substring(144, 155);

        return new GenericResponse<>(codigoCIC);
    }
    
    /**
     * =================================================================================================================
     * Registrar nuevo cliente
     *
     * @param request     {@link Customer}
     * @param httpHeaders cabeceras de aplicación
     * @return
     * @throws Throwable error
     */
    @Override
    public GenericResponse<?> createNewCustomerSIDG(CustomerSIDG request, HttpHeaders httpHeaders)
    throws Throwable
    {
        request.setHdLong(HEADER_LENGTH_SIUC.getValue());
        request.setHdTransaction(HEADER_LENGTH_SIUC.getValue());
        request.setHdUser(HEADER_USER.getValue());

        LocalDate now = LocalDate.now();
        
        DecimalFormat mFormat= new DecimalFormat("00");
        
        String mes =  mFormat.format(Double.valueOf(now.getMonthValue()));
        String dia =  mFormat.format(Double.valueOf(now.getDayOfMonth()));
        

        request.setHdYear(String.valueOf(now.getYear()));
        request.setHdMonth(mes);
        request.setHdDay(dia);

        //request.setHdSequential("99");
        request.setHdLong("SIUC");
        request.setHdTransaction("SIUC");
        request.setHdCodeHostResult("0000");
        request.setHdRequest("A");
        request.setHdForma("I");
        request.setHdType("CL");
        request.setHdProgram("       ");
        
        //AVargas - 20211015 - Inicio
        request.setHdOfficeOpen(validateStringLength(request.getInputCodeOffice(), 4));
        request.setHdOfficeProcess(validateStringLength(request.getInputCodeOffice(), 4));
        //AVargas - 20211015 - Fin
        
        // 6 - PJ, otros PN
        //request.setHdFill("PN");
        //AVargas - 20211020 - Inicio
        request.setHdFill(validateStringLength(request.getHdFill(),2));
        //AVargas - 20211020 - Fin
        
        //request.getCustomerAccountData().setCsubcta("000000");
        //AVargas - 20211021 - Inicio
        //request.getCustomerAccountData().setCdepend("0000");
        request.getCustomerAccountData().setCdepend(validateStringLength(request.getInputCodeOffice(), 4));
        //AVargas - 20211021 - Fin
        request.getCustomerAccountData().setTper("1");
        
        //seteo datacliente substring
        request.setCustomerCic(validateStringLength(request.getCustomerCic(), 12));
        request.setInputCodeOffice(validateStringLength(request.getInputCodeOffice(), 4));
        request.setOperationalCodeOffice(validateStringLength(request.getOperationalCodeOffice(), 4));
        request.setTypeDocument(validateStringLength(request.getTypeDocument(), 1));
        request.setNumberDocument(validateStringLength(request.getNumberDocument(), 12));
        request.setFlagUnderAge(validateStringLength(request.getFlagUnderAge(), 1));
        request.setFlagInterdicto(validateStringLength(request.getFlagInterdicto(), 1));
        request.setLastName(validateStringLength(request.getLastName(), 50));
        request.setMotherLastName(validateStringLength(request.getMotherLastName(), 50));
        request.setMarriedLastName(validateStringLength(request.getMarriedLastName(), 50));
        request.setFirstName(validateStringLength(request.getFirstName(), 50));
        request.setSecondName(validateStringLength(request.getSecondName(), 50));
        request.setCodeGender(validateStringLength(request.getCodeGender(), 1));
        request.setCodeCivilStatus(validateStringLength(request.getCodeCivilStatus(), 1));
        request.setYearsOfStudy(validateStringLength(request.getYearsOfStudy(), 1));
        request.setCustomerHeight(validateStringLength(request.getCustomerHeight(), 3));
        request.setFathersName(validateStringLength(request.getFathersName(), 15));
        request.setMothersName(validateStringLength(request.getMothersName(), 15));
        request.setCodeScaleOfInstruction(validateStringLength(request.getCodeScaleOfInstruction(), 1));
        request.setBirthdate(validateStringLength(request.getBirthdate(), 8));
        request.setCodeInternalCustomer(validateStringLength(request.getCodeInternalCustomer(), 6));
        request.setCodeInternalGroup(validateStringLength(request.getCodeInternalGroup(), 3));
        request.setStatusRelationship(validateStringLength(request.getStatusRelationship(), 1));
        request.setNumberPhone(validateStringLength(request.getNumberPhone(), 12));
        request.setCodeCustomerEmployment(validateStringLength(request.getCodeCustomerEmployment(), 5));
        request.setCodeEntityCustomer(validateStringLength(request.getCodeEntityCustomer(), 6));
        request.setCustomerFill(validateStringLength(request.getCustomerFill(), 16));
        //seteo dataclientedireccion substring
        CustomerAddress customerAddress = request.getCustomerAddress();
        customerAddress.setTypeRoad(validateStringLength(customerAddress.getTypeRoad(),1));
        customerAddress.setTypeAddress(validateStringLength(customerAddress.getTypeAddress(),20));
        customerAddress.setRoadDescription(validateStringLength(customerAddress.getRoadDescription(),60));
        customerAddress.setAddressNumber(validateStringLength(customerAddress.getAddressNumber(),30));
        customerAddress.setBlockNumber(validateStringLength(customerAddress.getBlockNumber(),30));
        customerAddress.setSquareNumber(validateStringLength(customerAddress.getSquareNumber(),30));
        customerAddress.setNumberLot(validateStringLength(customerAddress.getNumberLot(),30));
        customerAddress.setNumberFlat(validateStringLength(customerAddress.getNumberFlat(),30));
        customerAddress.setNumberDepartment(validateStringLength(customerAddress.getNumberDepartment(),30));
        customerAddress.setAddressReference(validateStringLength(customerAddress.getAddressReference(),45));
        customerAddress.setAddressFill(validateStringLength(customerAddress.getAddressFill(),5));
        request.setCustomerAddress(customerAddress);
        //seteo dataclientecuenta substring
        CustomerAccount customerAccount = request.getCustomerAccount();
        customerAccount.setCodeProduct(validateStringLength(customerAccount.getCodeProduct(), 2));
        customerAccount.setAccountNumber(validateStringLength(customerAccount.getAccountNumber(), 18));
        customerAccount.setTypeMoney(validateStringLength(customerAccount.getTypeMoney(), 3));
        customerAccount.setAccountShortName(validateStringLength(customerAccount.getAccountShortName(), 40));
        customerAccount.setAccountName(validateStringLength(customerAccount.getAccountName(), 150));
        customerAccount.setItemAddressDom(validateStringLength(customerAccount.getItemAddressDom(), 5));
        customerAccount.setItemAddressCore(validateStringLength(customerAccount.getItemAddressCore(), 5));
        customerAccount.setItemAddressWorkCenter(validateStringLength(customerAccount.getItemAddressWorkCenter(), 5));
        customerAccount.setItemAddressFiscal(validateStringLength(customerAccount.getItemAddressFiscal(), 5));
        customerAccount.setAccountSituationCode(validateStringLength(customerAccount.getAccountSituationCode(), 2));
        customerAccount.setTypeAccount(validateStringLength(customerAccount.getTypeAccount(), 2));
        customerAccount.setFlagAccountUOB(validateStringLength(customerAccount.getFlagAccountUOB(), 1));
        customerAccount.setTypeUbigee(validateStringLength(customerAccount.getTypeUbigee(), 1));
        customerAccount.setCodeUbigee(validateStringLength(customerAccount.getCodeUbigee(), 6));
        customerAccount.setCustomerAccountFill(validateStringLength(customerAccount.getCustomerAccountFill(), 25));
        customerAccount.setShippingMeansCode(validateStringLength(customerAccount.getShippingMeansCode(), 2));
        customerAccount.setTypeEvent(validateStringLength(customerAccount.getTypeEvent(), 2));
        customerAccount.setReasonEvent(validateStringLength(customerAccount.getReasonEvent(), 1));
        customerAccount.setCreationReasonCode(validateStringLength(customerAccount.getCreationReasonCode(), 3));
        customerAccount.setFlagBlacklist(validateStringLength(customerAccount.getFlagBlacklist(), 1));
        customerAccount.setFirstAgreementDate(validateStringLength(customerAccount.getFirstAgreementDate(), 8));
        request.setCustomerAccount(customerAccount);
        //seteo dataclientecuentadata substring
        CustomerAccountData customerAccountData = request.getCustomerAccountData();
        customerAccountData.setFapert(validateStringLength(customerAccountData.getFapert(),8));
        customerAccountData.setCsubcta(validateStringLength(customerAccountData.getCsubcta(),6));
        //customerAccountData.setCdepend(validateStringLength(customerAccountData.getCdepend(),4));
        customerAccountData.setCcliente(validateStringLength(customerAccountData.getCcliente(),6));
        customerAccountData.setCgrupo(validateStringLength(customerAccountData.getCgrupo(),3));
        customerAccountData.setCcuenta(validateStringLength(customerAccountData.getCcuenta(),11));
        customerAccountData.setCoinCode(validateStringLength(customerAccountData.getCoinCode(),3));
        customerAccountData.setNumberRUC(validateStringLength(customerAccountData.getNumberRUC(),11));
        customerAccountData.setTypeAper(validateStringLength(customerAccountData.getTypeAper(),1));
        customerAccountData.setCcal(validateStringLength(customerAccountData.getCcal(),2));
        customerAccountData.setTper(validateStringLength(customerAccountData.getTper(),1));
        customerAccountData.setNombresCli(validateStringLength(customerAccountData.getNombresCli(),70));
        customerAccountData.setDireccionCli(validateStringLength(customerAccountData.getDireccionCli(),70));
        customerAccountData.setAcortoCli(validateStringLength(customerAccountData.getAcortoCli(),35));
        customerAccountData.setNombresGRP(validateStringLength(customerAccountData.getNombresGRP(),70));
        customerAccountData.setDireccionGRP(validateStringLength(customerAccountData.getDireccionGRP(),70));
        customerAccountData.setAcortoGRP(validateStringLength(customerAccountData.getAcortoGRP(),35));
        customerAccountData.setAsig(validateStringLength(customerAccountData.getAsig(),15));
        customerAccountData.setCubi(validateStringLength(customerAccountData.getCubi(),6));
        customerAccountData.setCruc(validateStringLength(customerAccountData.getCruc(),11));
        customerAccountData.setCres(validateStringLength(customerAccountData.getCres(),1));
        customerAccountData.setCsec(validateStringLength(customerAccountData.getCsec(),3));
        customerAccountData.setLele(validateStringLength(customerAccountData.getLele(),8));
        customerAccountData.setNreg(validateStringLength(customerAccountData.getNreg(),15));
        customerAccountData.setCsbs(validateStringLength(customerAccountData.getCsbs(),10));
        customerAccountData.setCsct(validateStringLength(customerAccountData.getCsct(),3));
        customerAccountData.setCnac(validateStringLength(customerAccountData.getCnac(),1));
        customerAccountData.setCact(validateStringLength(customerAccountData.getCact(),4));
        customerAccountData.setCext(validateStringLength(customerAccountData.getCext(),9));
        customerAccountData.setTelf(validateStringLength(customerAccountData.getTelf(),7));
        customerAccountData.setAnex(validateStringLength(customerAccountData.getAnex(),4));
        customerAccountData.setNfax(validateStringLength(customerAccountData.getNfax(),7));
        customerAccountData.setFiller1(validateStringLength(customerAccountData.getFiller1(),11));
        customerAccountData.setCemp(validateStringLength(customerAccountData.getCemp(),1));
        customerAccountData.setCpos(validateStringLength(customerAccountData.getCpos(),6));
        customerAccountData.setCeco(validateStringLength(customerAccountData.getCeco(),4));
        customerAccountData.setAcact(validateStringLength(customerAccountData.getAcact(),17));
        customerAccountData.setAemp(validateStringLength(customerAccountData.getAemp(),17));
        customerAccountData.setAcontrib(validateStringLength(customerAccountData.getAcontrib(),30));
        customerAccountData.setAsec(validateStringLength(customerAccountData.getAsec(),17));
        customerAccountData.setAcortoCta2(validateStringLength(customerAccountData.getAcortoCta2(),35));
        customerAccountData.setSfin(validateStringLength(customerAccountData.getSfin(),15));
        customerAccountData.setFiller2(validateStringLength(customerAccountData.getFiller2(),150));
        request.setCustomerAccountData(customerAccountData);
        

        // ----------------------------------------------
        // Generar el request body gateway
        GatewayRequest gwRequest = new GatewayRequest();
        gwRequest.setLongitud(request.getHdLong());
        gwRequest.setTransId(request.getHdTransaction());

        gwRequest = objectConversionService.convertDtoToPlot(request, "", OPERATION_CREATE.getValue(), gwRequest);

        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient.runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);

        // -----------------------------------------
        // Validar la respuesta
        validateGatewayResponse(gwResponseEnt);
        CustomerSIDG customerResponse = objectConversionService
                .validateHeadersForPlot(new CustomerSIDG(), gwResponseEnt.getBody().getDatos(), "");  

        validatePlotResponse2(customerResponse);

        return new GenericResponse<>("OK");
    }
    
    /**
     * =================================================================================================================
     * Registrar nuevo cliente
     *
     * @param request     {@link Customer}
     * @param httpHeaders cabeceras de aplicación
     * @return
     * @throws Throwable error
     */
    @Override
    public GenericResponse<?> createNewCustomerCtaSIDG(CustomerCtaSIDG request, HttpHeaders httpHeaders)
    throws Throwable
    {
        request.setHdLong(HEADER_LENGTH_SIUC.getValue());
        request.setHdTransaction(HEADER_LENGTH_SIUC.getValue());
        request.setHdUser(HEADER_USER.getValue());

        LocalDate now = LocalDate.now();
        
        DecimalFormat mFormat= new DecimalFormat("00");
        
        String mes =  mFormat.format(Double.valueOf(now.getMonthValue()));
        String dia =  mFormat.format(Double.valueOf(now.getDayOfMonth()));
        

        request.setHdYear(String.valueOf(now.getYear()));
        request.setHdMonth(mes);
        request.setHdDay(dia);

        //request.setHdSequential("99");
        //Descomentar al subir
        request.setHdLong("SIUC");
        //Comentar al subir
        //request.setHdLong("9999");
        request.setHdTransaction("SIUC");
        request.setHdOfficeOpen("3111");
        request.setHdOfficeProcess("0068");
        request.setHdCodeHostResult("0000");
        request.setHdRequest("A");
        request.setHdForma("I");
        request.setHdType("CT");
        request.setHdProgram("");

        // 6 - PJ, otros PN
        //request.setHdFill("PN");
        request.setHdModification("");

        request.getCustomerAccountSIDGData().setCsubcta("050018");
        request.getCustomerAccountSIDGData().setCdepend("0068");
        //request.getCustomerAccountSIDGData().setCbco("51");
        
      //seteo datacliente substring
        request.setCustomerCic(validateStringLength(request.getCustomerCic(), 12));
        request.setInputCodeOffice(validateStringLength(request.getInputCodeOffice(), 4));
        request.setOperationalCodeOffice(validateStringLength(request.getOperationalCodeOffice(), 4));
        request.setTypeDocument(validateStringLength(request.getTypeDocument(), 1));
        request.setNumberDocument(validateStringLength(request.getNumberDocument(), 12));
        request.setFlagUnderAge(validateStringLength(request.getFlagUnderAge(), 1));
        request.setFlagInterdicto(validateStringLength(request.getFlagInterdicto(), 1));
        request.setLastName(validateStringLength(request.getLastName(), 50));
        request.setMotherLastName(validateStringLength(request.getMotherLastName(), 50));
        request.setMarriedLastName(validateStringLength(request.getMarriedLastName(), 50));
        request.setFirstName(validateStringLength(request.getFirstName(), 50));
        request.setSecondName(validateStringLength(request.getSecondName(), 50));
        request.setCodeGender(validateStringLength(request.getCodeGender(), 1));
        request.setCodeCivilStatus(validateStringLength(request.getCodeCivilStatus(), 1));
        request.setYearsOfStudy(validateStringLength(request.getYearsOfStudy(), 1));
        request.setCustomerHeight(validateStringLength(request.getCustomerHeight(), 3));
        request.setFathersName(validateStringLength(request.getFathersName(), 15));
        request.setMothersName(validateStringLength(request.getMothersName(), 15));
        request.setCodeScaleOfInstruction(validateStringLength(request.getCodeScaleOfInstruction(), 1));
        request.setBirthdate(validateStringLength(request.getBirthdate(), 8));
        request.setCodeInternalCustomer(validateStringLength(request.getCodeInternalCustomer(), 6));
        request.setCodeInternalGroup(validateStringLength(request.getCodeInternalGroup(), 3));
        request.setStatusRelationship(validateStringLength(request.getStatusRelationship(), 1));
        request.setNumberPhone(validateStringLength(request.getNumberPhone(), 12));
        request.setCodeCustomerEmployment(validateStringLength(request.getCodeCustomerEmployment(), 5));
        request.setCodeEntityCustomer(validateStringLength(request.getCodeEntityCustomer(), 6));
        request.setCustomerFill(validateStringLength(request.getCustomerFill(), 16));
        //seteo dataclientedireccion substring
        CustomerAddress customerAddress = request.getCustomerAddress();
        customerAddress.setTypeRoad(validateStringLength(customerAddress.getTypeRoad(),1));
        customerAddress.setTypeAddress(validateStringLength(customerAddress.getTypeAddress(),20));
        customerAddress.setRoadDescription(validateStringLength(customerAddress.getRoadDescription(),60));
        customerAddress.setAddressNumber(validateStringLength(customerAddress.getAddressNumber(),30));
        customerAddress.setBlockNumber(validateStringLength(customerAddress.getBlockNumber(),30));
        customerAddress.setSquareNumber(validateStringLength(customerAddress.getSquareNumber(),30));
        customerAddress.setNumberLot(validateStringLength(customerAddress.getNumberLot(),30));
        customerAddress.setNumberFlat(validateStringLength(customerAddress.getNumberFlat(),30));
        customerAddress.setNumberDepartment(validateStringLength(customerAddress.getNumberDepartment(),30));
        customerAddress.setAddressReference(validateStringLength(customerAddress.getAddressReference(),45));
        customerAddress.setAddressFill(validateStringLength(customerAddress.getAddressFill(),5));
        request.setCustomerAddress(customerAddress);
        //seteo dataclientecuenta substring
        CustomerAccount customerAccount = request.getCustomerAccount();
        customerAccount.setCodeProduct(validateStringLength(customerAccount.getCodeProduct(), 2));
        customerAccount.setAccountNumber(validateStringLength(customerAccount.getAccountNumber(), 18));
        customerAccount.setTypeMoney(validateStringLength(customerAccount.getTypeMoney(), 3));
        customerAccount.setAccountShortName(validateStringLength(customerAccount.getAccountShortName(), 40));
        customerAccount.setAccountName(validateStringLength(customerAccount.getAccountName(), 150));
        customerAccount.setItemAddressDom(validateStringLength(customerAccount.getItemAddressDom(), 5));
        customerAccount.setItemAddressCore(validateStringLength(customerAccount.getItemAddressCore(), 5));
        customerAccount.setItemAddressWorkCenter(validateStringLength(customerAccount.getItemAddressWorkCenter(), 5));
        customerAccount.setItemAddressFiscal(validateStringLength(customerAccount.getItemAddressFiscal(), 5));
        customerAccount.setAccountSituationCode(validateStringLength(customerAccount.getAccountSituationCode(), 2));
        customerAccount.setTypeAccount(validateStringLength(customerAccount.getTypeAccount(), 2));
        customerAccount.setFlagAccountUOB(validateStringLength(customerAccount.getFlagAccountUOB(), 1));
        customerAccount.setTypeUbigee(validateStringLength(customerAccount.getTypeUbigee(), 1));
        customerAccount.setCodeUbigee(validateStringLength(customerAccount.getCodeUbigee(), 6));
        customerAccount.setCustomerAccountFill(validateStringLength(customerAccount.getCustomerAccountFill(), 25));
        customerAccount.setShippingMeansCode(validateStringLength(customerAccount.getShippingMeansCode(), 2));
        customerAccount.setTypeEvent(validateStringLength(customerAccount.getTypeEvent(), 2));
        customerAccount.setReasonEvent(validateStringLength(customerAccount.getReasonEvent(), 1));
        customerAccount.setCreationReasonCode(validateStringLength(customerAccount.getCreationReasonCode(), 3));
        customerAccount.setFlagBlacklist(validateStringLength(customerAccount.getFlagBlacklist(), 1));
        customerAccount.setFirstAgreementDate(validateStringLength(customerAccount.getFirstAgreementDate(), 8));
        request.setCustomerAccount(customerAccount);
        //seteo dataclienteSIDGcuentadata substring
        CustomerAccountSIDGData customerAccountSIDGData = request.getCustomerAccountSIDGData();
        customerAccountSIDGData.setFapert(validateStringLength(customerAccountSIDGData.getFapert(),8));
        customerAccountSIDGData.setCsubcta(validateStringLength(customerAccountSIDGData.getCsubcta(),6));
        customerAccountSIDGData.setCdepend(validateStringLength(customerAccountSIDGData.getCdepend(),4));
        customerAccountSIDGData.setCcliente(validateStringLength(customerAccountSIDGData.getCcliente(),6));
        customerAccountSIDGData.setCgrupo(validateStringLength(customerAccountSIDGData.getCgrupo(),3));
        customerAccountSIDGData.setCcuenta(validateStringLength(customerAccountSIDGData.getCcuenta(),11));
        customerAccountSIDGData.setCmoneda2(validateStringLength(customerAccountSIDGData.getCmoneda2(),3));
        customerAccountSIDGData.setCruc(validateStringLength(customerAccountSIDGData.getCruc(),11));
        customerAccountSIDGData.setTipoAper(validateStringLength(customerAccountSIDGData.getTipoAper(),1));
        customerAccountSIDGData.setCtaAnterior(validateStringLength(customerAccountSIDGData.getCtaAnterior(),11));
        customerAccountSIDGData.setCorr(validateStringLength(customerAccountSIDGData.getCorr(),1));
        customerAccountSIDGData.setCbco(validateStringLength(customerAccountSIDGData.getCbco(),2));
        customerAccountSIDGData.setSiaf(validateStringLength(customerAccountSIDGData.getSiaf(),1));
        customerAccountSIDGData.setCubi3(validateStringLength(customerAccountSIDGData.getCubi3(),6));
        customerAccountSIDGData.setNombresCta(validateStringLength(customerAccountSIDGData.getNombresCta(),70));
        customerAccountSIDGData.setDireccionCta(validateStringLength(customerAccountSIDGData.getDireccionCta(),70));
        customerAccountSIDGData.setAcortoCta2(validateStringLength(customerAccountSIDGData.getAcortoCta2(),35));
        customerAccountSIDGData.setCubi4(validateStringLength(customerAccountSIDGData.getCubi4(),6));
        customerAccountSIDGData.setFiller1(validateStringLength(customerAccountSIDGData.getFiller1(),60));
        customerAccountSIDGData.setCubi5(validateStringLength(customerAccountSIDGData.getCubi5(),6));
        customerAccountSIDGData.setTelef3(validateStringLength(customerAccountSIDGData.getTelef3(),7));
        customerAccountSIDGData.setAnex3(validateStringLength(customerAccountSIDGData.getAnex3(),4));
        customerAccountSIDGData.setNfax3(validateStringLength(customerAccountSIDGData.getNfax3(),7));
        customerAccountSIDGData.setFiller2(validateStringLength(customerAccountSIDGData.getFiller2(),25));
        customerAccountSIDGData.setCcue(validateStringLength(customerAccountSIDGData.getCcue(),11));
        customerAccountSIDGData.setFven(validateStringLength(customerAccountSIDGData.getFven(),8));
        customerAccountSIDGData.setTrec(validateStringLength(customerAccountSIDGData.getTrec(),1));
        customerAccountSIDGData.setFapl (validateStringLength(customerAccountSIDGData.getFapl (),8));
        customerAccountSIDGData.setFliq(validateStringLength(customerAccountSIDGData.getFliq(),1));
        customerAccountSIDGData.setFest(validateStringLength(customerAccountSIDGData.getFest(),1));
        customerAccountSIDGData.setCest(validateStringLength(customerAccountSIDGData.getCest(),2));
        customerAccountSIDGData.setLcu(validateStringLength(customerAccountSIDGData.getLcu(),7));
        customerAccountSIDGData.setTacr(validateStringLength(customerAccountSIDGData.getTacr(),7));
        customerAccountSIDGData.setDcre(validateStringLength(customerAccountSIDGData.getDcre(),7));
        customerAccountSIDGData.setLpre(validateStringLength(customerAccountSIDGData.getLpre(),10));
        customerAccountSIDGData.setSobr(validateStringLength(customerAccountSIDGData.getSobr(),7));
        customerAccountSIDGData.setTpre(validateStringLength(customerAccountSIDGData.getTpre(),7));
        customerAccountSIDGData.setTgas(validateStringLength(customerAccountSIDGData.getTgas(),1));
        customerAccountSIDGData.setIgas(validateStringLength(customerAccountSIDGData.getIgas(),5));
        customerAccountSIDGData.setSgas(validateStringLength(customerAccountSIDGData.getSgas(),11));
        customerAccountSIDGData.setIact(validateStringLength(customerAccountSIDGData.getIact(),5));
        customerAccountSIDGData.setIpas(validateStringLength(customerAccountSIDGData.getIpas(),5));
        customerAccountSIDGData.setScom(validateStringLength(customerAccountSIDGData.getScom(),11));
        customerAccountSIDGData.setPort(validateStringLength(customerAccountSIDGData.getPort(),7));
        customerAccountSIDGData.setCint(validateStringLength(customerAccountSIDGData.getCint(),1));
        customerAccountSIDGData.setTcue(validateStringLength(customerAccountSIDGData.getTcue(),1));
        customerAccountSIDGData.setTcta(validateStringLength(customerAccountSIDGData.getTcta(),4));
        customerAccountSIDGData.setCtaPrin(validateStringLength(customerAccountSIDGData.getCtaPrin(),11));
        customerAccountSIDGData.setEsCtaprinPrev(validateStringLength(customerAccountSIDGData.getEsCtaprinPrev(),1));
        customerAccountSIDGData.setCalIntAcree(validateStringLength(customerAccountSIDGData.getCalIntAcree(),1));
        customerAccountSIDGData.setTipoTratam(validateStringLength(customerAccountSIDGData.getTipoTratam(),2));
        customerAccountSIDGData.setSectorCred(validateStringLength(customerAccountSIDGData.getSectorCred(),3));
        customerAccountSIDGData.setSectorPresu(validateStringLength(customerAccountSIDGData.getSectorPresu(),3));
        customerAccountSIDGData.setSectorConta(validateStringLength(customerAccountSIDGData.getSectorConta(),3));
        customerAccountSIDGData.setAreaContab(validateStringLength(customerAccountSIDGData.getAreaContab(),6));
        customerAccountSIDGData.setTcorr(validateStringLength(customerAccountSIDGData.getTcorr(),1));
        customerAccountSIDGData.setTcom(validateStringLength(customerAccountSIDGData.getTcom(),1));
        customerAccountSIDGData.setCdpr(validateStringLength(customerAccountSIDGData.getCdpr(),4));
        customerAccountSIDGData.setCte1(validateStringLength(customerAccountSIDGData.getCte1(),2));
        customerAccountSIDGData.setCte2(validateStringLength(customerAccountSIDGData.getCte2(),7));
        customerAccountSIDGData.setCte3(validateStringLength(customerAccountSIDGData.getCte3(),4));
        customerAccountSIDGData.setCpli(validateStringLength(customerAccountSIDGData.getCpli(),3));
        customerAccountSIDGData.setTcon6(validateStringLength(customerAccountSIDGData.getTcon6(),1));
        customerAccountSIDGData.setCmod6(validateStringLength(customerAccountSIDGData.getCmod6(),2));
        customerAccountSIDGData.setCsct6(validateStringLength(customerAccountSIDGData.getCsct6(),2));
        customerAccountSIDGData.setCsec6(validateStringLength(customerAccountSIDGData.getCsec6(),3));
        customerAccountSIDGData.setCact6(validateStringLength(customerAccountSIDGData.getCact6(),4));
        customerAccountSIDGData.setScre6(validateStringLength(customerAccountSIDGData.getScre6(),2));
        customerAccountSIDGData.setSdes6(validateStringLength(customerAccountSIDGData.getSdes6(),3));
        customerAccountSIDGData.setFaprob6(validateStringLength(customerAccountSIDGData.getFaprob6(),8));
        customerAccountSIDGData.setFejec6(validateStringLength(customerAccountSIDGData.getFejec6(),8));
        customerAccountSIDGData.setFvenc6(validateStringLength(customerAccountSIDGData.getFvenc6(),8));
        customerAccountSIDGData.setMonto6(validateStringLength(customerAccountSIDGData.getMonto6(),15));
        customerAccountSIDGData.setPlzo6(validateStringLength(customerAccountSIDGData.getPlzo6(),4));
        customerAccountSIDGData.setInter6(validateStringLength(customerAccountSIDGData.getInter6(),7));
        customerAccountSIDGData.setTtas6(validateStringLength(customerAccountSIDGData.getTtas6(),1));
        customerAccountSIDGData.setTasa6(validateStringLength(customerAccountSIDGData.getTasa6(),1));
        customerAccountSIDGData.setFflot6(validateStringLength(customerAccountSIDGData.getFflot6(),8));
        customerAccountSIDGData.setGasto6(validateStringLength(customerAccountSIDGData.getGasto6(),10));
        customerAccountSIDGData.setComis6(validateStringLength(customerAccountSIDGData.getComis6(),7));
        customerAccountSIDGData.setFfin6(validateStringLength(customerAccountSIDGData.getFfin6(),2));
        customerAccountSIDGData.setTlin6(validateStringLength(customerAccountSIDGData.getTlin6(),1));
        customerAccountSIDGData.setClin6(validateStringLength(customerAccountSIDGData.getClin6(),11));
        customerAccountSIDGData.setAcuerdo6(validateStringLength(customerAccountSIDGData.getAcuerdo6(),10));
        customerAccountSIDGData.setCser16(validateStringLength(customerAccountSIDGData.getCser16(),11));
        customerAccountSIDGData.setMarg6(validateStringLength(customerAccountSIDGData.getMarg6(),1));
        customerAccountSIDGData.setDividi6(validateStringLength(customerAccountSIDGData.getDividi6(),3));
        customerAccountSIDGData.setDiviso6(validateStringLength(customerAccountSIDGData.getDiviso6(),3));
        customerAccountSIDGData.setCsbs6(validateStringLength(customerAccountSIDGData.getCsbs6(),10));
        customerAccountSIDGData.setFiller3(validateStringLength(customerAccountSIDGData.getFiller3(),25));
        customerAccountSIDGData.setFiller4(validateStringLength(customerAccountSIDGData.getFiller4(),25));
        customerAccountSIDGData.setAbco(validateStringLength(customerAccountSIDGData.getAbco(),17));
        customerAccountSIDGData.setActaPrin(validateStringLength(customerAccountSIDGData.getActaPrin(),35));
        customerAccountSIDGData.setDistribucion(validateStringLength(customerAccountSIDGData.getDistribucion(),70));
        customerAccountSIDGData.setPag16(validateStringLength(customerAccountSIDGData.getPag16(),50));
        customerAccountSIDGData.setPag26(validateStringLength(customerAccountSIDGData.getPag26(),50));
        customerAccountSIDGData.setObservacion(validateStringLength(customerAccountSIDGData.getObservacion(),45));
        customerAccountSIDGData.setObj16(validateStringLength(customerAccountSIDGData.getObj16(),40));
        customerAccountSIDGData.setObj26(validateStringLength(customerAccountSIDGData.getObj26(),40));
        customerAccountSIDGData.setFiller5(validateStringLength(customerAccountSIDGData.getFiller5(),3));
        request.setCustomerAccountSIDGData(customerAccountSIDGData);

        // ----------------------------------------------
        // Generar el request body gateway
        GatewayRequest gwRequest = new GatewayRequest();
        gwRequest.setLongitud(request.getHdLong());
        gwRequest.setTransId(request.getHdTransaction());

        gwRequest = objectConversionService.convertDtoToPlot(request, "", OPERATION_CREATE.getValue(), gwRequest);

        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient.runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);

        // -----------------------------------------
        // Validar la respuesta
        validateGatewayResponse(gwResponseEnt);
        CustomerCtaSIDG customerResponse = objectConversionService
                .validateHeadersForPlot(new CustomerCtaSIDG(), gwResponseEnt.getBody().getDatos(), "");

        validatePlotResponse2(customerResponse);
        
        String trama = gwResponseEnt.getBody().getDatos();
        String accountNumber = trama.substring(836, 853);

        return new GenericResponse<>(accountNumber);
    }

    /**
     * =================================================================================================================
     * Actualizar información de cliente
     *
     * @param clienteContactabilidad {@link ClienteContactabilidad}
     * @param httpHeaders            cabeceras de aplicación
     * @return
     * @throws Throwable
     */
    @Override
    public GenericResponse<?> createNewCustomerContactInformation(ClienteContactabilidad clienteContactabilidad,
                                                                  HttpHeaders httpHeaders) throws Throwable
    {
        clienteContactabilidad.setHdLong(HEADER_LENGTH_SIUC.getValue());
        clienteContactabilidad.setHdTran(HEADER_LENGTH_SIUC.getValue());

        clienteContactabilidad.setHdCodTran("0720");
        clienteContactabilidad.setAccion("A");

        // ----------------------------------------------
        // Generar el request body gateway
        GatewayRequest gwRequest = new GatewayRequest();
        gwRequest.setLongitud(clienteContactabilidad.getHdLong());
        gwRequest.setTransId(clienteContactabilidad.getHdTran());

        gwRequest = objectConversionService
                .convertDtoToPlot(clienteContactabilidad, "", OPERATION_CREATE.getValue(), gwRequest);

        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient
                .runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);

        // Validar la respuesta
        validateGatewayResponse(gwResponseEnt);

        return new GenericResponse<>("OK");
    }
    
    private String validateStringLength(String cadena, int longitud) {    	
    	if(cadena!= null) {
    		return (cadena.length()>=longitud)?cadena.substring(0, longitud) : cadena;
    	}
    	return "";
    }

    private String leftPadString(String cadena, int length) {
    	
    	StringBuilder sb = new StringBuilder();
    	
    	for(int i=0; i<length; i++) {
    		sb.append(' ');
    	}
    	return cadena + sb.substring(cadena.length());
    }
}
