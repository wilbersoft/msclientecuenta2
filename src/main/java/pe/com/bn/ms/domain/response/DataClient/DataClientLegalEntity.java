package pe.com.bn.ms.domain.response.DataClient;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Response de clientes juridica")
@Data

public class DataClientLegalEntity implements Serializable{
	@ApiModelProperty(value = "") private String	siglas;
	@ApiModelProperty(value = "") private String	razonSocial;
	@ApiModelProperty(value = "") private String	fechaConstitucion;
	@ApiModelProperty(value = "") private String	codOficinaRegistral;
	@ApiModelProperty(value = "") private String	codSubOficinaRegistral;
	@ApiModelProperty(value = "") private String	regPubId;
	@ApiModelProperty(value = "") private String	regPubRegPubl;
	@ApiModelProperty(value = "") private String	regPubNumPartidaFicha;
	@ApiModelProperty(value = "") private String	regPubNumFolio;
	@ApiModelProperty(value = "") private String	regPubNumTomo;
	@ApiModelProperty(value = "") private String	regPubReportado;
	@ApiModelProperty(value = "") private String	paginaWeb;
	@ApiModelProperty(value = "") private String	operadorNumTelefCelular;
	@ApiModelProperty(value = "") private String	numTelefCelular;
	@ApiModelProperty(value = "") private String	emailEmpresa;
	@ApiModelProperty(value = "") private String	emailOpcional;
}
