package pe.com.bn.ms.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
public class ParameterAddressRequest
{
    private String CIC;
    private String numCorre;

    public ParameterAddressRequest(String CIC, String numCorre)
    {
        this.CIC      = StringUtils.isNotBlank(CIC) ? CIC : null;
        this.numCorre = StringUtils.isNotBlank(numCorre) ? numCorre : null;
    }


}
