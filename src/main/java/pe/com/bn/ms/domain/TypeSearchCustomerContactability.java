package pe.com.bn.ms.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TypeSearchCustomerContactability
{
    private String cic;
}

