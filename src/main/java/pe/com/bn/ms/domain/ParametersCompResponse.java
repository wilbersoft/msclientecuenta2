package pe.com.bn.ms.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParametersCompResponse implements Serializable
{
    private String bdUser;
    private String bdUrl;
    private String bdDriver;
    private String bdPass;
    private String mensaje;
    private String result;

}
