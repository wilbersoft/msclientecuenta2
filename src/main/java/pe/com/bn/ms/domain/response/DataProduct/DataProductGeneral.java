package pe.com.bn.ms.domain.response.DataProduct;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Response de productos en la busqueda")
@Data

public class DataProductGeneral implements Serializable{
	@ApiModelProperty(value = "") private String productCode;
	@ApiModelProperty(value = "") private String productDescription;
	@ApiModelProperty(value = "") private String accountNumber;
	@ApiModelProperty(value = "") private String accountType;
	@ApiModelProperty(value = "") private String accountTypeDescription;
	@ApiModelProperty(value = "") private String addressCoreDescription;
	@ApiModelProperty(value = "") private String fullNameAccount;
	@ApiModelProperty(value = "") private String stateAccount;
	@ApiModelProperty(value = "") private String stateAccountDescription;
	@ApiModelProperty(value = "") private String accountSituationCode;
	@ApiModelProperty(value = "") private String accountSituationDescription;
	@ApiModelProperty(value = "") private String accountingState;
	@ApiModelProperty(value = "") private String accountingStateDescription;
	@ApiModelProperty(value = "") private String openingDate;
	@ApiModelProperty(value = "") private String cancellationDate;
	@ApiModelProperty(value = "") private String amountAvailable;
	@ApiModelProperty(value = "") private String flagAccountUOB;
	@ApiModelProperty(value = "") private String accountCCI;
	@ApiModelProperty(value = "") private String internalEntityCode;
	@ApiModelProperty(value = "") private String internalEntityDescription;
	@ApiModelProperty(value = "") private String accountNature;
	@ApiModelProperty(value = "") private String accountNatureDescription;
	@ApiModelProperty(value = "") private String shippingMeansCode;
}
