package pe.com.bn.ms.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @description cabeceras de la aplicación
 */
@Getter
@Setter
public class Headers implements Serializable
{
    private String application;
    private String applicationCode;
    private String channel;
    private String channelCode;
    private String macAddress;
    private String ip;
    private String device;
}
