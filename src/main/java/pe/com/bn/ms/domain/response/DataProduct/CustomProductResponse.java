package pe.com.bn.ms.domain.response.DataProduct;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
//Custom Product response general para invocar a los metodos dentro de la API

@ApiModel(description = "Datos Principales del Producto")

public class CustomProductResponse implements Serializable{
	private String cic;
	private String typeDocument;
	private String descriptionTypeDocument;
	private String documentNumber;
	private String fullName;
	private String numberItems;
	private List<DataProductGeneral> productList;
}
