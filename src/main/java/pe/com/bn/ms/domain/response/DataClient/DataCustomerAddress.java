package pe.com.bn.ms.domain.response.DataClient;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Response de clientes direccion")
@Data

public class DataCustomerAddress implements Serializable {
	@ApiModelProperty(value = "")
	private String itemAddress;
	@ApiModelProperty(value = "")
	private String typeAddress;
	@ApiModelProperty(value = "")
	private String typeRoad;
	@ApiModelProperty(value = "")
	private String descriptionRoad;
	@ApiModelProperty(value = "")
	private String addressNumber;
	@ApiModelProperty(value = "")
	private String blockNumber;
	@ApiModelProperty(value = "")
	private String squareNumber;
	@ApiModelProperty(value = "")
	private String lotNumber;
	@ApiModelProperty(value = "")
	private String flatNumber;
	@ApiModelProperty(value = "")
	private String interiorDptNumber;
	@ApiModelProperty(value = "")
	private String reportedAddress;
	@ApiModelProperty(value = "")
	private String locationReference;
	@ApiModelProperty(value = "")
	private String postOfficeBox;
	@ApiModelProperty(value = "")
	private String addressPhoneNumber;
	@ApiModelProperty(value = "")
	private String addressAnnexPhone;
	@ApiModelProperty(value = "")
	private String addressFaxPhone;
	@ApiModelProperty(value = "")
	private String ubigeoType;
	@ApiModelProperty(value = "")
	private String ubigeoCode;
	@ApiModelProperty(value = "")
	private String flagDomiciledVisit;
	@ApiModelProperty(value = "")
	private String domiciledVisitDate;
	@ApiModelProperty(value = "")
	private String homeVisitDescription;
	@ApiModelProperty(value = "")
	private String zipCode;
	@ApiModelProperty(value = "")
	private String ubigeoAhorrosCode;
	@ApiModelProperty(value = "")
	private String ubigeoCtaCteCode;
	@ApiModelProperty(value = "")
	private String ubigeoReniecCode;
	@ApiModelProperty(value = "")
	private String ubigeoSunatCode;
	@ApiModelProperty(value = "")
	private String inputDate;
	@ApiModelProperty(value = "")
	private String lastChangeDate;
	@ApiModelProperty(value = "")
	private String stateAddress;
}
