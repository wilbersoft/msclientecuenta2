package pe.com.bn.ms.domain;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class TypeSearchCustomer
{
    private String typeSearch;
    private String cic;
    private String typeDocument;
    private String numberDocument;
    private String typeProduct;
    private String numberProduct;
    private String externalEntityFlag;
    private String userApplication;

    public TypeSearchCustomer(String typeSearch, String cic, String typeDocument, String numberDocument,
                              String typeProduct, String numberProduct, String externalEntityFlag, String userApplication)
    {
        this.typeSearch         = StringUtils.isNotBlank(typeSearch) ? typeSearch : null;
        this.cic                = StringUtils.isNotBlank(cic) ? cic : null;
        this.typeDocument       = StringUtils.isNotBlank(typeDocument) ? typeDocument : null;
        this.numberDocument     = StringUtils.isNotBlank(numberDocument) ? numberDocument : null;
        this.typeProduct        = StringUtils.isNotBlank(typeProduct) ? typeProduct : null;
        this.numberProduct      = StringUtils.isNotBlank(numberProduct) ? numberProduct : null;
        this.externalEntityFlag = StringUtils.isNotBlank(externalEntityFlag) ? externalEntityFlag : null;
        this.userApplication = StringUtils.isNotBlank(userApplication) ? userApplication : null;
    }
 
}
