package pe.com.bn.ms.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.ClienteContactabilidad;
import pe.com.bn.ms.dto.Customer;
import pe.com.bn.ms.dto.CustomerCtaSIDG;
import pe.com.bn.ms.dto.Direccion;
import pe.com.bn.ms.dto.CustomerSIDG;
import pe.com.bn.ms.service.customer.CustomerCreateService;

/**
 * Descripción: Api solo para creación (C -> CREATE)
 */
@RestController
@RequestMapping("/msdataclients")
@Api(tags = "Apis de creación - DataClients",
     value = "Creación de nuevos clientes y derivados. ")
public class CustomerCreateApi
{
    private final CustomerCreateService customerService;

    @Autowired
    public CustomerCreateApi(CustomerCreateService customerService)
    {
        this.customerService = customerService;
    }

    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para:
     * - Añadir un persona (PN o PJ)
     *
     * @param customer objeto cliente, example: .../resources/json/customer-create.json
     * @return {@link }
     * @throws Exception GlobalExceptionHandler
     */
    @ApiOperation(value = "Añadir un persona (PN o PJ). ", response = String.class)
    @PostMapping(value = {"/client"},
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createNewCustomer(@ApiParam(name = "PersonaCliente", value = "Json PersonaCliente")
                                               @RequestHeader HttpHeaders httpHeaders,
                                               @RequestBody Customer customer)
    throws Throwable
    {
        GenericResponse<?> genericResponse = customerService
                .createNewCustomer(customer, httpHeaders);

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
    
    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para:
     * - Añadir un persona SIDG (PN o PJ)
     *
     * @param customer objeto cliente, example: .../resources/json/customer-create.json
     * @return {@link }
     * @throws Exception GlobalExceptionHandler
     */
    @ApiOperation(value = "Añadir un persona (PN o PJ). ", response = String.class)
    @PostMapping(value = {"/clientSIDG"},
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createNewCustomer(@ApiParam(name = "PersonaClienteSIDG", value = "Json PersonaCliente")
                                               @RequestHeader HttpHeaders httpHeaders,
                                               @RequestBody CustomerSIDG customer)
    throws Throwable
    {
        GenericResponse<?> genericResponse = customerService
                .createNewCustomerSIDG(customer, httpHeaders);

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
    
    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para:
     * - Añadir un persona SIDG (PN o PJ)
     *
     * @param customer objeto cliente, example: .../resources/json/customer-create.json
     * @return {@link }
     * @throws Exception GlobalExceptionHandler
     */
    @ApiOperation(value = "Añadir un persona (PN o PJ). ", response = String.class)
    @PostMapping(value = {"/clientCtaSIDG"},
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createNewCustomer(@ApiParam(name = "PersonaClienteCtaSIDG", value = "Json PersonaClienteCta")
                                               @RequestHeader HttpHeaders httpHeaders,
                                               @RequestBody CustomerCtaSIDG customer)
    throws Throwable
    {	ObjectMapper mapper = new ObjectMapper();
    	String json = mapper.writeValueAsString(customer);
    	System.out.println("customer: "+json);
        GenericResponse<?> genericResponse = customerService
                .createNewCustomerCtaSIDG(customer, httpHeaders);

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para:
     * - Añadir una dirección de Cliente
     *
     * @param CIC       código interno de cliente
     * @param direccion objeto dirección
     * @return OK
     * @throws Exception GlobalExceptionHandler
     */
    @ApiOperation(value = "Añadir una dirección de Cliente. ", response = String.class)
    @PostMapping(value = {"/address/{CIC}"},
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createClientAddress(
            @ApiParam(value = "Código interno de cliente") @PathVariable String CIC,
            @ApiParam(name = "Direccion", value = "Json Direccion") @RequestBody Direccion direccion)
    throws Exception
    {
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para:
     * - Actualizar los datos de contactabilidad de un cliente
     *
     * @param CIC                    código interno de cliente
     * @param clienteContactabilidad cliente contactabilidad
     * @return OK
     * @throws Exception GlobalExceptionHandler
     */
    @ApiOperation(value = "Actualizar los datos de Contactabilidad de un cliente. ", response = String.class)
    @PostMapping(value = {"/client/contactability/{CIC}"},
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createNewCustomerContactInformation(
            @ApiParam(value = "Código interno de cliente") @PathVariable String CIC,
            @ApiParam(name = "ClienteContactabilidad", value = "Json ClienteContactabilidad")
            @RequestBody ClienteContactabilidad clienteContactabilidad,
            @RequestHeader HttpHeaders httpHeaders)
    throws Throwable
    {
        clienteContactabilidad.setCic(CIC);

        GenericResponse<?> genericResponse = customerService
                .createNewCustomerContactInformation(clienteContactabilidad, httpHeaders);

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);

    }

}
