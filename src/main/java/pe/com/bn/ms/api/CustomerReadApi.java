package pe.com.bn.ms.api;

import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.ParameterAddressRequest;
import pe.com.bn.ms.domain.TypeSearchCustomer;
import pe.com.bn.ms.domain.TypeSearchCustomerContactability;
import pe.com.bn.ms.domain.response.DataClient.CustomResponse;
import pe.com.bn.ms.dto.ClienteContactabilidad;
import pe.com.bn.ms.dto.Direccion;
import pe.com.bn.ms.exception.base4XX.TypeSearchNotFoundException;
import pe.com.bn.ms.model.ClientSwg;
import pe.com.bn.ms.model.ClienteContactabilidadSwg;
import pe.com.bn.ms.model.DireccionSwg;
import pe.com.bn.ms.model.PersonaClienteSwg;
import pe.com.bn.ms.service.address.AddressService;
import pe.com.bn.ms.service.customer.CustomerReadService;
import pe.com.bn.ms.util.Shared.DataUtil;

import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_CIC;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_DOCUMENT;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_PRODUCT;
import static pe.com.bn.ms.util.functions.DateTimePicker.gatewayTimestamp;

import java.util.List;
import java.util.ArrayList;

/**
 * @author prov_destrada
 * @description Api solo para consultas (R -> READ)
 */
@RestController
@RequestMapping("/msdataclients/client")
@Api(tags = "Apis de consulta - DataClients", value = "Consulta de clientes y derivados. ")
public class CustomerReadApi {
	private final CustomerReadService customerService;
	private final AddressService addressService;

	@Autowired
	public CustomerReadApi(CustomerReadService customerService, AddressService addressService) {
		this.customerService = customerService;
		this.addressService = addressService;
	}

	/* Prueba12 */

	/**
	 * =================================================================================================================
	 * Descripción: Gestor de datos de Persona (Cliente o Usuario).
	 * <p>
	 * Ejemplo: requiere
	 *
	 * @param userApplication    Usuario que realiza la consulta (usuario de 4
	 *                           letras)
	 * @param cic                Cic del Cliente.
	 * @param tipDoc             Tipo de Documento de busqueda
	 * @param numDoc             Numero de Documento busqueda
	 * @param typeProduct        Tipo de Producto busqueda
	 * @param numProduct         Numero de Producto busqueda
	 * @param typeSearch         tipo de búsqueda
	 * @param flagEntidadExterna flag de consulta
	 * @return GenericResponse<PersonaCliente>
	 */



	@ApiOperation(value = "Consulta de datos de Persona (Cliente o Usuario). ", response = ClientSwg.class)
	@GetMapping(value = { "/v1/search/{typeSearch}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getClient(@ApiParam(value = "Tipo de Busqueda") @PathVariable String typeSearch,
			@ApiParam(value = "Usuario") @RequestParam(required = true) String userApplication,
			@ApiParam(value = "Código interno cliente") @RequestParam(required = false) String cic,
			@ApiParam(value = "Tipo de documento") @RequestParam(required = false) String typeDoc,
			@ApiParam(value = "Número de documento") @RequestParam(required = false) String numDoc,
			@ApiParam(value = "Tipo de producto") @RequestParam(required = false) String typeProduct,
			@ApiParam(value = "Número de producto") @RequestParam(required = false) String numProduct,
			@ApiParam(value = "Flag de consulta") @RequestParam(required = false) String flagEntidadExterna,
			@RequestHeader HttpHeaders httpHeaders) throws Throwable {

		List<String> errorMessages = new ArrayList<>();

		DataUtil dataUtil = new DataUtil();

		// Validacion de los parametros de busqueda
		dataUtil.validateParameter(userApplication, "userApplication", 4, 6, false, errorMessages);

		dataUtil.validateParameter(cic, "cic", 2, 12, false, errorMessages);
		dataUtil.validateParameter(typeDoc, "typeDoc", 1, 1, false, errorMessages);
		dataUtil.validateParameter(numDoc, "numDoc", 2, 12, true, errorMessages);
		dataUtil.validateParameter(typeProduct, "typeProduct", 1, 2, false, errorMessages);
		dataUtil.validateParameter(numProduct, "numProduct", 3, 18, true, errorMessages);
		dataUtil.validateParameter(flagEntidadExterna, "flagEntidadExterna", 1, 1, false, errorMessages);

		// Si hay errores, devolver un HttpStatus.BAD_REQUEST con el mensaje de error
		if (!errorMessages.isEmpty()) {
			String errorMessage = String.join("\n", errorMessages);
			return ResponseEntity.badRequest().body(errorMessage);
		}

		// Objeto que encapsula los criterios de búsqueda del cliente
		TypeSearchCustomer clientRequest = new TypeSearchCustomer(typeSearch.toUpperCase(), cic, typeDoc, numDoc,
				typeProduct, numProduct, flagEntidadExterna, userApplication);

		GenericResponse<CustomResponse> genericResponse;

		if (SEARCH_CIC.getType().equals(typeSearch)) {
			//// Verifica si el parámetro 'cic' no está vacío
			if (dataUtil.checkSearchParameter(cic)) {
				genericResponse = customerService.findCustomerByCic(httpHeaders, clientRequest);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("El CIC es obligatorio para la búsqueda");
			}
		}

		else if (SEARCH_NUMBER_DOCUMENT.getType().equals(typeSearch)) {
			//// Verifica si los parámetros 'typeDoc' y 'numDoc' no están vacíos
			if (dataUtil.checkSearchParameter(typeDoc, numDoc)) {
				genericResponse = customerService.findCustomerByTypeAndNumberDocument(httpHeaders, clientRequest);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("El numero y tipo de documento es obligatorio para la búsqueda");
			}
		}

		else if (SEARCH_NUMBER_PRODUCT.getType().equals(typeSearch)) {
			//// Verifica si los parámetros 'typeProduct' y 'numProduct' no están vacíos
			if (dataUtil.checkSearchParameter(typeProduct, numProduct)) {
				genericResponse = customerService.findCustomerByTypeAndNumberProduct(httpHeaders, clientRequest);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("El numero y tipo de producto es obligatorio para la búsqueda");
			}
		}

		else {
			// throw new TypeSearchNotFoundException("TypeSearch: " + typeSearch);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tipo de búsqueda no encontrada");
		}

		if (genericResponse != null && genericResponse.getData() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No hay datos vinculados con la búsqueda");
		}

		return new ResponseEntity<>(genericResponse, HttpStatus.OK);

	}




}
