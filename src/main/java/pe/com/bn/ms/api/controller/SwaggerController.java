package pe.com.bn.ms.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SwaggerController
{
    @RequestMapping(value = "/swagger", method = RequestMethod.GET)
    public String swaggerHome()
    {
        return "redirect:/swagger-ui.html#/";
    }
}
