package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @created 16/11/2020 - 05:24 p. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class CustomerAddress implements Serializable
{
	//DATACLIENTEDIRECCION
    @ApiModelProperty(value = "") private String typeRoad;
    @ApiModelProperty(value = "") private String typeAddress;
    @ApiModelProperty(value = "") private String roadDescription;
    @ApiModelProperty(value = "") private String addressNumber;
    @ApiModelProperty(value = "") private String blockNumber;
    @ApiModelProperty(value = "") private String squareNumber;
    @ApiModelProperty(value = "") private String numberLot;
    @ApiModelProperty(value = "") private String numberFlat;
    @ApiModelProperty(value = "") private String numberDepartment;
    @ApiModelProperty(value = "") private String addressReference;
    @ApiModelProperty(value = "") private String reportedAddress;
    @ApiModelProperty(value = "") private String addressFill;
}
