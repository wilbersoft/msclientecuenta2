package pe.com.bn.ms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author prov_destrada
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GatewayRequest implements Serializable
{
    private String datos;
    private String filler;
    private String longitud;
    private String transId;
}
