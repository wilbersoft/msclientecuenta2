package pe.com.bn.ms.dto;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static pe.com.bn.ms.util.functions.DateTimePicker.gatewayTimestamp;

import org.springframework.http.HttpHeaders;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.com.bn.ms.util.Shared.DataUtil;

@Data
@NoArgsConstructor
@SuppressWarnings("all")
@ApiModel(description = "Persona, este es el cliente. ")
public class PersonaClient {

	// ------------------------------------------
    // Parametro de los HEADERS
	private String hdLong;
	private String hdTran;
	private String hdTimestamp;
	private String hdCodTran1Format;
	private String hdCodUser;
	private String hdDesRestFormat;
	private String hdCodCanal;
	private String hdCodTerm;
	private String hdCodRetLog;
	private String hdDesRetLog;
	
	// ------------------------------------------
    // Parametros del BODY de entrada
	
	private String inTdcmtoId;
	private String inNdcmtoIdFormat;
	private String inTipoBusFormat;
	private String inTipoResultFormat;
	private String inUltctaFormat;
	private String inCicFormat;
	private String inCodProdFormat;
	private String inNumProdFormat;
	private String inNumCorDirFormat;
	private String inNtarjetaInputFormat;
	private String inTdocumAdFormat;
	private String inFiller;
	
	// ------------------------------------------
    // Parametros del BODY de salida
	
	private String ClteCinternoCic;   
	private String ClteCoficIngreso;  
	private String ClteCoficOperac;   
	private String ClteFingresoBco;
	private String ClteCejectNeg;  
	private String des;        
	private String ClteCsectorEcon;       
	private String ClteCsectorEconDes;
	private String ClteCactEconCiuu;        
	private String ClteCactEconCiuuDes;
	private String ClteTclte;        
	private String ClteTclteDes;        
	private String ClteTbanca;        
	private String ClteTbancaDes;       
	private String ClteCsegmentoClte;        
	private String ClteCsegmentoClteDes;        
	private String ClteTdcmtoId;        
	private String ClteTdcmtoIdDes;        
	private String ClteNdcmtoId;       
	private String ClteAnombRazClte;        
	private String CLTE_CtipoPersona;          
	private String ClteCtipoPersonaDes;        
	private String ClteNtelefono;         
	private String ClteNanexoTelef;         
	private String ClteNfax;        
	private String ClteCagenteBcr;        
	private String ClteCclteSbs;        
	private String ClteCmancomunoSbs;       
	private String ClteClasifDeubn;       
	private String ClteFiller01;       
	private String ClteCclasifDeuSb;       
	private String ClteCclasifDeuSbDes;
	private String ClteCmagnitudClte;       		
	private String ClteCmagnitudClteDes;      
	private String ClteGdatoComplem;      
	private String ClteCrelLabBco;        
	private String ClteCaccionistBco;        
	private String ClteCatrasoDeu;        
	private String CLTE_CATRASO_DEU_DES;         
	private String CLTE_CRIESG_CAMB;        
	private String CLTE_CRIESG_CAMB_DES;       
	private String CLTE_GCHEQ_SFOND_BCO;         
	private String CLTE_GCHEQ_SFOND_SBS;         
	private String CLTE_GLAVADO_ACTIVO;        
	private String CLTE_GCLAVE_INTERNE;        
	private String CLTE_GTARJETA_MULTI;        
	private String CLTE_GPREST_MULTIR;        
	private String CLTE_GGARANTE_MULTI;        
	private String CLTE_GVISITA_DOMIC;        
	private String CLTE_GNACIONALIDAD;        
	private String CLTE_GMENOR_EDAD;        
	private String CLTE_GACEPTANTE;        
	private String CLTE_GCOMERCIO_EXT;        
	private String CLTE_CRESIDENC_PAIS;        
	private String CLTE_CRESIDENC_PAIS_DES;        
	private String CLTE_CPAIS_RESID;        
	private String CLTE_CPAIS_RESID_DES;        
	private String CLTE_CSECTOR_PUB;        
	private String CLTE_CSECTOR_PUB_DES;        
	private String CLTE_GFALLECIDO;        
	private String CLTE_CENTIDAD_INTER;       
	private String CLTE_CENTIDAD_INTER_DES;       
	private String CLTE_CCLIENTE_INTER;       
	private String CLTE_CGRUPO_INTERF;      
	private String CLTE_TDCMTO_REPORT;        
	private String CLTE_NDCMTO_REPORT;        
	private String CLTE_ANOMBRES_REPOR;       
	private String CLTE_GINTERDICTO;       
	private String CLTE_FULT_LAVADO;         
	private String CLTE_CCLIENTE_ANT;  
	private String  CLTE_CGRUPO_ANT;        
	private String CLTE_FULTIMA_ACT;        
	private String CLTE_SING_ANUALES;        
	private String CLTE_CCLASIF_DEU_SALIN;        
	private String CLTE_CCLASIF_DEU_ALIN;        
	private String CLTE_TDOC_COMP;        
	private String CLTE_NDOC_COMP;        
	private String CLTE_TAPERTURA_CLTE;        
	private String CLTE_GPERS_EXP_PUB;        
	private String CLTE_GCONTRACTUAL;        
	private String CLTE_GMENSAJE_TXT;        
	private String CLTE_GOBLIG_INFORMAR;        
	private String FILLER02;
	private String CLTE_UBI_CLTE;        
	private String CLTE_DIR_CLTE;       
	private String CLTEN_CIC;        
	private String CLTEN_APE_PATERNO;        
	private String CLTEN_APE_MATERNO;       
	private String CLTEN_APE_DE_CASADA;       
	private String CLTEN_APRIMERO;        
	private String CLTEN_ASEGUNDO;        
	private String CLTEN_CPAIS_NAC;        
	private String CLTEN_CPAIS_NAC_DES;       
	private String CLTEN_FNACIMIENTO;       
	private String CLTEN_TUBIGEO_NAC;       
	private String CLTEN_CUBIGEO_NAC;        
	private String CLTEN_CSEXO;        
	private String CLTEN_CESTADO_CIVIL;       
	private String CLTEN_CESTADO_CIVIL_DES;         
	private String CLTEN_NTELEFONO_PERS;        
	private String CLTEN_EMAIL_PERSONAL;        
	private String CLTEN_EMAIL_TRABAJO;       
	private String CLTEN_CGRADO_INSTRUC;       
	private String CLTEN_CPROFESION;       
	private String CLTEN_CPROFESION_DES;
	private String CLTEN_COCUPACION;     
	private String CLTEN_COCUPACION_DES;      
	private String CLTEN_COCUP_REPORT;       
	private String CLTEN_GPERS_NEGOCIO;       
	private String CLTEN_NRUC_PERS_NEG;        
	private String CLTEN_NDEPENDIENTES;        
	private String CLTEN_ALOCAL_NAC;        
	private String CLTEN_NANOS_EST;        
	private String CLTEN_NESTATURA;        
	private String CLTEN_ANOMBRE_PADRE;        
	private String CLTEN_ANOMBRE_MADRE;       
	private String CLTEJ_CINTERNO_CIC;        
	private String CLTEJ_ASIGLAS;       
	private String CLTEJ_ARAZON_SOCIAL;       
	private String CLTEJ_FCONSTITUCION;       
	private String CLTEJ_REG_PUB_OFIC;       
	private String CLTEJ_REG_PUB_SUBOFI;       
	private String CLTEJ_REG_PUB_ID;        
	private String CLTEJ_REG_PUB_NPART;        
	private String CLTEJ_REG_PUB_FOLIO;        
	private String CLTEJ_REG_PUB_TOMO;        
	private String CLTEJ_REG_PUB_REPORT;        
	private String CLTEJ_DPAGINA_WEB;       
	private String CLTEJ_NTELEFONO_CELULAR;        
	private String CLTEJ_EMAIL_EMPRESA;        
	private String CLTEJ_EMAIL_OPCIONAL;        
	private String CLTEJ_FILLER;        
	private String CLTEDIR_CINTERNO_CIC;        
	private String CLTEDIR_NITEM_DIREC;        
	private String CLTEDIR_TDIRECCION;        
	private String CLTEDIR_TVIA;        
	private String CLTEDIR_AVIA;       
	private String CLTEDIR_NNUMERO;        
	private String CLTEDIR_NBLOQUE;        
	private String CLTEDIR_NMANZANA;        
	private String CLTEDIR_NLOTE;        
	private String CLTEDIR_NPISO;        
	private String CLTEDIR_NDPTO_INTER;        
	private String CLTEDIR_DREPORTADA;      
	private String CLTEDIR_APTO_REFEREN;        
	private String CLTEDIR_NAPARTADO_POST;        
	private String CLTEDIR_NTELEFONO_DIR;        
	private String CLTEDIR_NANEXO_DIR;       
	private String CLTEDIR_NFAX;       
	private String CLTEDIR_TUBIGEO;        
	private String CLTEDIR_CUBIGEO;       
	private String CLTEDIR_GVISITA;        
	private String CLTEDIR_FVISITA;        
	private String CLTEDIR_AVISITA;         
	private String CLTEDIR_CPOSTAL;       
	private String CLTEDIR_CUBIGEO_AHORROS;         
	private String CLTEDIR_CUBIGEO_CTA_CTE;       
	private String CLTEDIR_CUBIGEO_RENIEC;        
	private String CLTEDIR_CUBIGEO_SUNAT;      
	private String CLTEDIR_FINGRESO_DIR;       
	private String CLTEDIR_FULTMODIF_DIR;       
	private String CLTEDIR_BSITUACION;        
	private String CLTEDIR_FILLER;
	private String CLTEIND_CINTERNO_CIC;        
	private String CLTEIND_CESTADO_CVC;         
	private String CLTEIND_CRAZON_ALTA;        
	private String CLTEIND_CSITC_ECON;        
	private String CLTEIND_FPRIM_ACRDO;        
	private String CLTEIND_GPROTEC_DATOS;        
	private String CLTEIND_GNO_PUBLICIDAD;         
	private String CLTEIND_GSESION_TERC;        
	private String CLTEIND_GACCESO_RSTRG;        
	private String CLTEIND_GVIP;        
	private String CLTEIND_GLST_NGR;        
	private String CLTEIND_TRAT_ESP;        
	private String CLTEIND_BLQ_CL;         
	private String CLTEIND_GTOCKEN;        
	private String CLTEIND_GBCELULAR;       
	private String CLTE_FECHA_PACTO;        
	private String CLTE_ADEPARTAMENTO_AHOR;        
	private String CLTE_APROVINCIA_AHOR;       
	private String CLTE_ADISTRITO_AHOR;        
	private String CLTE_ADEPARTAMENTO_CTCT;          
	private String CLTE_APROVINCIA_CTCT;         
	private String CLTE_ADISTRITO_CTCT;        
	private String CLTE_FILLER_DIR;
	
	public PersonaClient(HttpHeaders headers) throws Throwable
    {
        this.hdTimestamp         = gatewayTimestamp();
        this.hdCodUser           = "DJRS";
    }
	
	
}
