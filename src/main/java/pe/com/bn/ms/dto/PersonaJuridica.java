package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Persona jurídica, es hijo de PersonaCliente. ")
public class PersonaJuridica extends PersonaNatural implements Serializable
{
    @ApiModelProperty(name = "AcronymCompany", value = "Siglas/Nombre Comercial") private String acronymCompany;
    @ApiModelProperty(name = "NameCompany",
                      value = "Razon Social") private                                     String nameCompany;
    @ApiModelProperty(name = "CompanyCreationDate",
                      value = "Fecha Constitucion") private                               String companyCreationDate;
    @ApiModelProperty(name = "CodOficinaRegistral",
                      value = "Codigo Oficina Registral") private                         String codOficinaRegistral;
    @ApiModelProperty(name = "CodSubOficinaRegistral",
                      value = "Codigo SubOficina Registral") private                      String codSubOficinaRegistral;
    @ApiModelProperty(name = "RegPubId",
                      value = "Codigo Id Reg Pub") private                                String regPubId;
    @ApiModelProperty(name = "RegPubNumPartidaFicha",
                      value = "Id Reg Publico - Numero Partida o Ficha") private          String regPubNumPartidaFicha;
    @ApiModelProperty(name = "RegPubNumFolio",
                      value = "Id Reg Publico - Numero de Folio") private                 String regPubNumFolio;
    @ApiModelProperty(name = "RegPubNumTomo",
                      value = "Id Reg Publico - Numero de Tomo") private                  String regPubNumTomo;
    @ApiModelProperty(name = "RegPubReportado",
                      value = "Id Reg Publico - Reportado") private                       String regPubReportado;
    @ApiModelProperty(name = "WebPage",
                      value = "Pagina Web") private                                       String webPage;
    @ApiModelProperty(name = "CorporationEmail",
                      value = "Número de teléfono celular") private                       String corporationEmail;
    @ApiModelProperty(name = "OptionalEmail",
                      value = "Correo de la empresa") private                             String optionalEmail;
}
