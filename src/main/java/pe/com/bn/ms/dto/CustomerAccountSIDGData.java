package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @created 16/11/2020 - 05:24 p. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class CustomerAccountSIDGData implements Serializable
{
	//BODY-DATACUENTA
	@ApiModelProperty(value = "") private String fapert;
	@ApiModelProperty(value = "") private String csubcta;
	@ApiModelProperty(value = "") private String cdepend;
	@ApiModelProperty(value = "") private String ccliente;
	@ApiModelProperty(value = "") private String cgrupo;
	@ApiModelProperty(value = "") private String ccuenta;
	@ApiModelProperty(value = "") private String cmoneda2;
	@ApiModelProperty(value = "") private String cruc;
	@ApiModelProperty(value = "") private String tipoAper;
	@ApiModelProperty(value = "") private String ctaAnterior;
	@ApiModelProperty(value = "") private String corr;
	@ApiModelProperty(value = "") private String cbco;
	@ApiModelProperty(value = "") private String siaf;
	@ApiModelProperty(value = "") private String cubi3;
	@ApiModelProperty(value = "") private String nombresCta;
	@ApiModelProperty(value = "") private String direccionCta;
	@ApiModelProperty(value = "") private String acortoCta2;
	@ApiModelProperty(value = "") private String cubi4;
	@ApiModelProperty(value = "") private String filler1;
	@ApiModelProperty(value = "") private String cubi5;
	@ApiModelProperty(value = "") private String telef3;
	@ApiModelProperty(value = "") private String anex3;
	@ApiModelProperty(value = "") private String nfax3;
	@ApiModelProperty(value = "") private String filler2;
	@ApiModelProperty(value = "") private String ccue;
	@ApiModelProperty(value = "") private String fven;
	@ApiModelProperty(value = "") private String trec;
	@ApiModelProperty(value = "") private String fapl ;
	@ApiModelProperty(value = "") private String fliq;
	@ApiModelProperty(value = "") private String fest;
	@ApiModelProperty(value = "") private String cest;
	@ApiModelProperty(value = "") private String lcu;
	@ApiModelProperty(value = "") private String tacr;
	@ApiModelProperty(value = "") private String dcre;
	@ApiModelProperty(value = "") private String lpre;
	@ApiModelProperty(value = "") private String sobr;
	@ApiModelProperty(value = "") private String tpre;
	@ApiModelProperty(value = "") private String tgas;
	@ApiModelProperty(value = "") private String igas;
	@ApiModelProperty(value = "") private String sgas;
	@ApiModelProperty(value = "") private String iact;
	@ApiModelProperty(value = "") private String ipas;
	@ApiModelProperty(value = "") private String scom;
	@ApiModelProperty(value = "") private String port;
	@ApiModelProperty(value = "") private String cint;
	@ApiModelProperty(value = "") private String tcue;
	@ApiModelProperty(value = "") private String tcta;
	@ApiModelProperty(value = "") private String ctaPrin;
	@ApiModelProperty(value = "") private String esCtaprinPrev;
	@ApiModelProperty(value = "") private String calIntAcree;
	@ApiModelProperty(value = "") private String tipoTratam;
	@ApiModelProperty(value = "") private String sectorCred;
	@ApiModelProperty(value = "") private String sectorPresu;
	@ApiModelProperty(value = "") private String sectorConta;
	@ApiModelProperty(value = "") private String areaContab;
	@ApiModelProperty(value = "") private String tcorr;
	@ApiModelProperty(value = "") private String tcom;
	@ApiModelProperty(value = "") private String cdpr;
	@ApiModelProperty(value = "") private String cte1;
	@ApiModelProperty(value = "") private String cte2;
	@ApiModelProperty(value = "") private String cte3;
	@ApiModelProperty(value = "") private String cpli;
	@ApiModelProperty(value = "") private String tcon6;
	@ApiModelProperty(value = "") private String cmod6;
	@ApiModelProperty(value = "") private String csct6;
	@ApiModelProperty(value = "") private String csec6;
	@ApiModelProperty(value = "") private String cact6;
	@ApiModelProperty(value = "") private String scre6;
	@ApiModelProperty(value = "") private String sdes6;
	@ApiModelProperty(value = "") private String faprob6;
	@ApiModelProperty(value = "") private String fejec6;
	@ApiModelProperty(value = "") private String fvenc6;
	@ApiModelProperty(value = "") private String monto6;
	@ApiModelProperty(value = "") private String plzo6;
	@ApiModelProperty(value = "") private String inter6;
	@ApiModelProperty(value = "") private String ttas6;
	@ApiModelProperty(value = "") private String tasa6;
	@ApiModelProperty(value = "") private String fflot6;
	@ApiModelProperty(value = "") private String gasto6;
	@ApiModelProperty(value = "") private String comis6;
	@ApiModelProperty(value = "") private String ffin6;
	@ApiModelProperty(value = "") private String tlin6;
	@ApiModelProperty(value = "") private String clin6;
	@ApiModelProperty(value = "") private String acuerdo6;
	@ApiModelProperty(value = "") private String cser16;
	@ApiModelProperty(value = "") private String marg6;
	@ApiModelProperty(value = "") private String dividi6;
	@ApiModelProperty(value = "") private String diviso6;
	@ApiModelProperty(value = "") private String csbs6;
	@ApiModelProperty(value = "") private String filler3;
	@ApiModelProperty(value = "") private String filler4;
	@ApiModelProperty(value = "") private String abco;
	@ApiModelProperty(value = "") private String actaPrin;
	@ApiModelProperty(value = "") private String distribucion;
	@ApiModelProperty(value = "") private String pag16;
	@ApiModelProperty(value = "") private String pag26;
	@ApiModelProperty(value = "") private String observacion;
	@ApiModelProperty(value = "") private String obj16;
	@ApiModelProperty(value = "") private String obj26;
	@ApiModelProperty(value = "") private String filler5;


}
