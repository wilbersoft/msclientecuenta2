package pe.com.bn.ms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

/**
 * @author prov_destrada
 * @created 22/10/2020 - 06:14 p. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class Product implements Serializable
{
    // ------------------------------------------
    // Cabeceras de producto - INPUT
    @JsonProperty(access = WRITE_ONLY) private String hdLong;
    @JsonProperty(access = WRITE_ONLY) private String hdTran;
    @JsonProperty(access = WRITE_ONLY) private String hdTimestamp;
    @JsonProperty(access = WRITE_ONLY) private String hdCodTran;
    @JsonProperty(access = WRITE_ONLY) private String hdCodUser;
    @JsonProperty(access = WRITE_ONLY) private String hdCodeHostResult;
    @JsonProperty(access = WRITE_ONLY) private String hdMessageHostResult;
    @JsonProperty(access = WRITE_ONLY) private String hdCodCanal;
    @JsonProperty(access = WRITE_ONLY) private String hdCodTerm;
    @JsonProperty(access = WRITE_ONLY) private String hdCodRetLog;
    @JsonProperty(access = WRITE_ONLY) private String hdDesRetLog;

    // ------------------------------------------
    // Cuerpo de producto - INPUT
    @JsonProperty(access = WRITE_ONLY) private String typeDocumentIn;
    @JsonProperty(access = WRITE_ONLY) private String numberDocumentIn;

    @JsonProperty(access = WRITE_ONLY) private String typeSearch;
    @JsonProperty(access = WRITE_ONLY) private String typeResult;
    @JsonProperty(access = WRITE_ONLY) private String ultCta;
    @JsonProperty(access = WRITE_ONLY) private String cicInSecond;
    @JsonProperty(access = WRITE_ONLY) private String codProd;
    @JsonProperty(access = WRITE_ONLY) private String numProd;
    @JsonProperty(access = WRITE_ONLY) private String numCorDir;
    @JsonProperty(access = WRITE_ONLY) private String numTarjeta;
    @JsonProperty(access = WRITE_ONLY) private String tipoDocumento;
    @JsonProperty(access = WRITE_ONLY) private String filler;

    // ------------------------------------------
    // Cuerpo de producto - OUTPUT
    private String cic;
    private String typeDocument;
    private String descriptionTypeDocument;
    private String documentNumber;
    private String fullName;
    private String numberItems;

    private Collection<ProductData> products = new ArrayList<>();

}
