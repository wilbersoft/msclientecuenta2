package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @created 16/11/2020 - 05:24 p. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class CustomerAccountDetail implements Serializable
{
	//BODY-DATACUENTA
    @ApiModelProperty(value = "") private String typeEventAccount;
    @ApiModelProperty(value = "") private String businessExecutiveCode;
    @ApiModelProperty(value = "") private String typePerson;
    @ApiModelProperty(value = "") private String typeDocumentReport;
    @ApiModelProperty(value = "") private String numberDocumentReport;
    @ApiModelProperty(value = "") private String fullNameReport;
    @ApiModelProperty(value = "") private String typeCustomer;
    @ApiModelProperty(value = "") private String flagCountryResidence;
    @ApiModelProperty(value = "") private String flagNationality;
    @ApiModelProperty(value = "") private String codeCountryOfResidence;
    @ApiModelProperty(value = "") private String codeRelationshipWorkingBco;
    @ApiModelProperty(value = "") private String codeShareholderBco;
    @ApiModelProperty(value = "") private String codeEconomicSector;
    @ApiModelProperty(value = "") private String codeEconomicActivity;
    @ApiModelProperty(value = "") private String codeDebtorClass;
    @ApiModelProperty(value = "") private String codeRisk;
    @ApiModelProperty(value = "") private String customerMagnitudeCode;
    @ApiModelProperty(value = "") private String descriptionDebtorClassSBS;
    @ApiModelProperty(value = "") private String annualProfit;
    @ApiModelProperty(value = "") private String numberRUC;
    @ApiModelProperty(value = "") private String companyName;
    @ApiModelProperty(value = "") private String companyAcronym;
    @ApiModelProperty(value = "") private String codeOfficeRegistry;
    @ApiModelProperty(value = "") private String subCodeOfficeRegistry;
    @ApiModelProperty(value = "") private String companyCreationDate;
    @ApiModelProperty(value = "") private String registryPublicID;
    @ApiModelProperty(value = "") private String registryPublicNumberCertificate;
    @ApiModelProperty(value = "") private String registryPublicNumberFolio;
    @ApiModelProperty(value = "") private String registryPublicNumberVolume;
    @ApiModelProperty(value = "") private String registryPublicReported;
    @ApiModelProperty(value = "") private String referenceDocument;
    @ApiModelProperty(value = "") private String tdCmToAh;
    @ApiModelProperty(value = "") private String codeDegreeEducationAccount;
    @ApiModelProperty(value = "") private String codeProfessionAccount;
    @ApiModelProperty(value = "") private String codeDelayAccount;
    @ApiModelProperty(value = "") private String codeDebtorClassification;
    @ApiModelProperty(value = "") private String codeDebtorClassificationAlign;
    @ApiModelProperty(value = "") private String typeDocumentComplementary;
    @ApiModelProperty(value = "") private String numberDocumentComplementary;
    @ApiModelProperty(value = "") private String flagPersonWithBusiness;
    @ApiModelProperty(value = "") private String codeCountryOfBirth;
    @ApiModelProperty(value = "") private String flagDeceased;
    @ApiModelProperty(value = "") private String codeDebtorClassSBS;
    @ApiModelProperty(value = "") private String typeOfBank;
    @ApiModelProperty(value = "") private String segmentClientCode;
    @ApiModelProperty(value = "") private String codePublicSector;
    @ApiModelProperty(value = "") private String numberDependents;
    @ApiModelProperty(value = "") private String codeEmployment;
    @ApiModelProperty(value = "") private String flagVisitHome;
    @ApiModelProperty(value = "") private String accountHomePhone;
    @ApiModelProperty(value = "") private String accountWorkingPhone;
    @ApiModelProperty(value = "") private String accountWorkingPhoneAnnex;
    @ApiModelProperty(value = "") private String cellphone;
    @ApiModelProperty(value = "") private String personalEmail;
    @ApiModelProperty(value = "") private String optionalEmail;
    @ApiModelProperty(value = "") private String webPage;
    @ApiModelProperty(value = "") private String regPubReportado02;
    @ApiModelProperty(value = "") private String gPersonExpPub;
    @ApiModelProperty(value = "") private String reported;
    @ApiModelProperty(value = "") private String tUbigeeBirth;
    @ApiModelProperty(value = "") private String cUbigeeBirth;
    @ApiModelProperty(value = "") private String accountRelated;
    @ApiModelProperty(value = "") private String accountCustomerCic;
    @ApiModelProperty(value = "") private String relation;
    @ApiModelProperty(value = "") private String startRelationship;
    @ApiModelProperty(value = "") private String endRelationship;
    @ApiModelProperty(value = "") private String porcPartC;
    @ApiModelProperty(value = "") private String documented;
    @ApiModelProperty(value = "") private String relatedObservations;
    @ApiModelProperty(value = "") private String indicatorBIN;
    @ApiModelProperty(value = "") private String tUbigeeSavingsAccount;
    @ApiModelProperty(value = "") private String cUbigeeSavingsAccount;
    @ApiModelProperty(value = "") private String tUbigeeCustomerAccount;
    @ApiModelProperty(value = "") private String cUbigeeCustomerAccount;
    @ApiModelProperty(value = "") private String accountFill;
}
