package pe.com.bn.ms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpHeaders;

import java.io.Serializable;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static pe.com.bn.ms.util.functions.DateTimePicker.gatewayTimestamp;

@Data
@NoArgsConstructor
@SuppressWarnings("all")
@ApiModel(description = "Persona, este es el cliente. ")
public class PersonaCliente implements Serializable
{
    // ------------------------------------------
    // Headers INPUT
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdLong;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdTran;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdTimestamp;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodTran;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodUser;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodeHostResult;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdMessageHostResult;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodCanal;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodTerm;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodRetLog;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdDesRetLog;

    // ------------------------------------------
    // Body INPUT
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String typeDocument;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String numDocument;

    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String tipoBus;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String tipoResult;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String ultcta;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String cicInSecond;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String codProd;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String numProd;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String numCorDir;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String numTarjeta;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String tipoDocumento;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String filler;

    // ------------------------------------------
    // Body OUTPUT
    @ApiModelProperty(value = "") private String cinternoCicOut;
    @ApiModelProperty(value = "") private String officeInputCode;
    @ApiModelProperty(value = "") private String officeOperationalCode;
    @ApiModelProperty(value = "") private String admissionBankDate;
    @ApiModelProperty(value = "") private String businessExecutiveCode;
    @ApiModelProperty(value = "") private String businessExecutiveDesc;
    @ApiModelProperty(value = "") private String economicSectorCode;
    @ApiModelProperty(value = "") private String economicSectorDesc;
    @ApiModelProperty(value = "") private String economicActivityCode;
    @ApiModelProperty(value = "") private String economicActivityDesc;
    @ApiModelProperty(value = "") private String clientType;
    @ApiModelProperty(value = "") private String clientTypeDesc;
    @ApiModelProperty(value = "") private String bankingType;
    @ApiModelProperty(value = "") private String clientTypeDescSecond;
    @ApiModelProperty(value = "") private String segmentClientCode;
    @ApiModelProperty(value = "") private String segmentClientDesc;
    @ApiModelProperty(value = "") private String documentType;
    @ApiModelProperty(value = "") private String documentTypeDesc;
    @ApiModelProperty(value = "") private String documentNumber;
    @ApiModelProperty(value = "") private String fullName;
    @ApiModelProperty(value = "") private String personType;
    @ApiModelProperty(value = "") private String personTypeDesc;
    @ApiModelProperty(value = "") private String workingPhone;
    @ApiModelProperty(value = "") private String annexPhone;
    @ApiModelProperty(value = "") private String homePhone;
    @ApiModelProperty(value = "") private String BCRAgentCode;
    @ApiModelProperty(value = "") private String SBSClientCode;
    @ApiModelProperty(value = "") private String SBSJointCode;
    @ApiModelProperty(value = "") private String debtorClasBNCode;
    @ApiModelProperty(value = "") private String debtorClasSBSCode;
    @ApiModelProperty(value = "") private String debtorClasSBSDesc;
    @ApiModelProperty(value = "") private String clientMagnitudeCode;
    @ApiModelProperty(value = "") private String clientMagnitudeDesc;
    @ApiModelProperty(value = "") private String flagSupplementaryData;
    @ApiModelProperty(value = "") private String relLabBcoCode;
    @ApiModelProperty(value = "") private String accionistBcoCode;
    @ApiModelProperty(value = "") private String atrasoDeuCode;
    @ApiModelProperty(value = "") private String atrasoDeuDesc;
    @ApiModelProperty(value = "") private String riesgCambCode;
    @ApiModelProperty(value = "") private String riesgCambDesc;
    @ApiModelProperty(value = "") private String flagCheqSinFondoBco;
    @ApiModelProperty(value = "") private String flagCheqSinFondoSBS;
    @ApiModelProperty(value = "") private String flagLavadoDinero;
    @ApiModelProperty(value = "") private String flagInternetKey;
    @ApiModelProperty(value = "") private String flagMultiredCard;
    @ApiModelProperty(value = "") private String flagoMultiredLoan;
    @ApiModelProperty(value = "") private String flagMultiredGuarantor;
    @ApiModelProperty(value = "") private String flagHomeVisite;
    @ApiModelProperty(value = "") private String flagNationality;
    @ApiModelProperty(value = "") private String flagUnderAge;
    @ApiModelProperty(value = "") private String flagAcceptor;
    @ApiModelProperty(value = "") private String flagExternalTrade;
    @ApiModelProperty(value = "") private String flagCountryResidence;
    @ApiModelProperty(value = "") private String DescCountryResidence;
    @ApiModelProperty(value = "") private String codeCountryOfResidence;
    @ApiModelProperty(value = "") private String descCountryOfResidence;
    @ApiModelProperty(value = "") private String codePublicSector;
    @ApiModelProperty(value = "") private String descPublicSector;
    @ApiModelProperty(value = "") private String flagDead;
    @ApiModelProperty(value = "") private String codeEntityClient;
    @ApiModelProperty(value = "") private String descEntityClient;
    @ApiModelProperty(value = "") private String internalClientCode;
    @ApiModelProperty(value = "") private String internalGroupCode;
    @ApiModelProperty(value = "") private String documentTypeReport;
    @ApiModelProperty(value = "") private String documentNumberReport;
    @ApiModelProperty(value = "") private String fullNameReport;
    @ApiModelProperty(value = "") private String flagInterdicto;
    @ApiModelProperty(value = "") private String lastLavadoActivoDate;
    @ApiModelProperty(value = "") private String lastClientCode;
    @ApiModelProperty(value = "") private String lastGroupCode;
    @ApiModelProperty(value = "") private String lastChangeDate;
    @ApiModelProperty(value = "") private String annualProfit;
    @ApiModelProperty(value = "") private String clasifDebtorCode;
    @ApiModelProperty(value = "") private String clasifDebtorAlignedCode;
    @ApiModelProperty(value = "") private String docComplementaryType;
    @ApiModelProperty(value = "") private String docComplementaryNum;
    @ApiModelProperty(value = "") private String openingClientType;
    @ApiModelProperty(value = "") private String flagPEP;
    @ApiModelProperty(value = "") private String flagContractual;
    @ApiModelProperty(value = "") private String flagTextMessage;
    @ApiModelProperty(value = "") private String flagForcedToInform;
    @ApiModelProperty(value = "") private String lastName01;
    @ApiModelProperty(value = "") private String lastName02;
    @ApiModelProperty(value = "") private String marriedName;
    @ApiModelProperty(value = "") private String firstName;
    @ApiModelProperty(value = "") private String secondName;
    @ApiModelProperty(value = "") private String countryOfBirthCode;
    @ApiModelProperty(value = "") private String countryOfBirthDesc;
    @ApiModelProperty(value = "") private String birthDate;
    @ApiModelProperty(value = "") private String ubigeoBirthType;
    @ApiModelProperty(value = "") private String ubigeoBirthCode;
    @ApiModelProperty(value = "") private String sexCode;
    @ApiModelProperty(value = "") private String civilStatusCode;
    @ApiModelProperty(value = "") private String civilStatusDesc;
    @ApiModelProperty(value = "") private String numberMobile;
    @ApiModelProperty(value = "") private String personalEmail;
    @ApiModelProperty(value = "") private String workEmail;
    @ApiModelProperty(value = "") private String degreeOfInstructionCode;
    @ApiModelProperty(value = "") private String professionCode;
    @ApiModelProperty(value = "") private String professionDesc;
    @ApiModelProperty(value = "") private String occupationCode;
    @ApiModelProperty(value = "") private String occupationDesc;
    @ApiModelProperty(value = "") private String occupationCodeReported;
    @ApiModelProperty(value = "") private String flagBusinessPerson;
    @ApiModelProperty(value = "") private String numberRUC;
    @ApiModelProperty(value = "") private String dependentNum;
    @ApiModelProperty(value = "") private String localityBirth;
    @ApiModelProperty(value = "") private String yearOfStudy;
    @ApiModelProperty(value = "") private String heightClient;
    @ApiModelProperty(value = "") private String fatherName;
    @ApiModelProperty(value = "") private String motherName;
    @ApiModelProperty(value = "") private String acronymCompany;
    @ApiModelProperty(value = "") private String nameCompany;
    @ApiModelProperty(value = "") private String CompanyCreationDate;
    @ApiModelProperty(value = "") private String codOficinaRegistral;
    @ApiModelProperty(value = "") private String codSubOficinaRegistral;
    @ApiModelProperty(value = "") private String regPubId;
    @ApiModelProperty(value = "") private String regPubNumPartidaFicha;
    @ApiModelProperty(value = "") private String regPubNumFolio;
    @ApiModelProperty(value = "") private String regPubNumTomo;
    @ApiModelProperty(value = "") private String regPubReportado;
    @ApiModelProperty(value = "") private String webPage;
    @ApiModelProperty(value = "") private String celularPhone;
    @ApiModelProperty(value = "") private String CorporationEmail;
    @ApiModelProperty(value = "") private String optionalEmail;
    @ApiModelProperty(value = "") private String cinternoCicSecond;
    @ApiModelProperty(value = "") private String itemAddress;
    @ApiModelProperty(value = "") private String typeAddress;
    @ApiModelProperty(value = "") private String typeRoad;
    @ApiModelProperty(value = "") private String descriptionRoad;
    @ApiModelProperty(value = "") private String addressNumber;
    @ApiModelProperty(value = "") private String blockNumber;
    @ApiModelProperty(value = "") private String squareNumber;
    @ApiModelProperty(value = "") private String lotNumber;
    @ApiModelProperty(value = "") private String flatNumber;
    @ApiModelProperty(value = "") private String interiorDptNumber;
    @ApiModelProperty(value = "") private String reportedAddress;
    @ApiModelProperty(value = "") private String locationReference;
    @ApiModelProperty(value = "") private String postOfficeBox;
    @ApiModelProperty(value = "") private String addressPhoneNumber;
    @ApiModelProperty(value = "") private String addressAnnexPhone;
    @ApiModelProperty(value = "") private String addressFaxPhone;
    @ApiModelProperty(value = "") private String ubigeoType;
    @ApiModelProperty(value = "") private String ubigeoCode;
    @ApiModelProperty(value = "") private String flagDomiciledVisit;
    @ApiModelProperty(value = "") private String domiciledVisitDate;
    @ApiModelProperty(value = "") private String homeVisitDescription;
    @ApiModelProperty(value = "") private String zipCode;
    @ApiModelProperty(value = "") private String ubigeoAhorrosCode;
    @ApiModelProperty(value = "") private String ubigeoCtaCteCode;
    @ApiModelProperty(value = "") private String ubigeoReniecCode;
    @ApiModelProperty(value = "") private String ubigeoSunatCode;
    @ApiModelProperty(value = "") private String inputDate;
    @ApiModelProperty(value = "") private String lastChangeDateSecond;
    @ApiModelProperty(value = "") private String stateAddress;
    @ApiModelProperty(value = "") private String lifeCycleVidaClientState;
    @ApiModelProperty(value = "") private String creationReasonCode;
    @ApiModelProperty(value = "") private String economicSituationCode;
    @ApiModelProperty(value = "") private String firstAgreementDate;
    @ApiModelProperty(value = "") private String flagDataProtected;
    @ApiModelProperty(value = "") private String flagPublicity;
    @ApiModelProperty(value = "") private String flagCesionOthers;
    @ApiModelProperty(value = "") private String flagRestrictedAccess;
    @ApiModelProperty(value = "") private String flagVIP;
    @ApiModelProperty(value = "") private String flagBlackList;
    @ApiModelProperty(value = "") private String flagSpecialTrat;
    @ApiModelProperty(value = "") private String flagClteBlock;
    @ApiModelProperty(value = "") private String flagToken;
    @ApiModelProperty(value = "") private String flagBankMovil;
    @ApiModelProperty(value = "") private String firstAgreementDateSecond;

    // ----------------------------------------------
    // Constructores

    public PersonaCliente(HttpHeaders headers) throws Throwable
    {
        this.hdTimestamp         = gatewayTimestamp();
        this.hdCodUser           = "DJRS";
        this.hdCodeHostResult    = "00000";
        this.hdMessageHostResult = "0000000000000000000000000";
    }

}
