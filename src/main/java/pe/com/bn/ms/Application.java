package pe.com.bn.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import lombok.Data;

@EnableAspectJAutoProxy
@EnableFeignClients("pe.com.bn.ms.payload")
@SpringBootApplication
@Data
public class Application
{

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
        
    }
       
}
