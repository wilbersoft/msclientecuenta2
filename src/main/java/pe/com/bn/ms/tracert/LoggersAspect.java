package pe.com.bn.ms.tracert;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import pe.com.bn.ms.tracert.logger.LoggerService;
import pe.com.bn.ms.tracert.rabbit.RabbitService;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * @author prov_destrada
 * @created 26/10/2020 - 01:01 p. m.
 * @project ms-dataclients
 */
@Log4j2
@Aspect
@Component
public class LoggersAspect
{
    @Autowired(required = false) private HttpServletRequest httpRequest;

    @Value("${app.comp.rabbit}") private String valuemqn;

    private final LoggerService                  loggerService;
    private final RabbitService                  rabbitService;
    private final ConfigurableApplicationContext ctx;

    private UUID uuid;

    @Autowired
    public LoggersAspect(ConfigurableApplicationContext ctx, LoggerService loggerService,
                         RabbitService rabbitService)
    {
        this.ctx           = ctx;
        this.loggerService = loggerService;
        this.rabbitService = rabbitService;
    }

    /**
     * =================================================================================================================
     * Realiza la trazabilidad a nivel de:
     * - Capa web, se trazan las apis
     * - Capa de negocio, se trazan los métodos que contienen lógica de negocio
     * <p>
     * Este interceptor no realiza envío de eventos o bitácoras
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @return dependiendo de las entradas que reciba el método, estas son devueltas
     * @throws Throwable error
     */
    @Around(value = "pe.com.bn.ms.tracert.SystemArchitecture.inWebLayer() || " +
                    "pe.com.bn.ms.tracert.SystemArchitecture.inBusinessServiceLayer()")
    public Object loggerAroundTimeWebAndBusinessLayer(ProceedingJoinPoint joinPoint) throws Throwable
    {
        uuid = UUID.randomUUID();
        return loggerService.aroundLoggerTime(joinPoint, uuid, true);
    }

    /**
     * =================================================================================================================
     * Realiza la trazabilidad a nivel de:
     * - Capa de negocio, se trazan los métodos que contienen lógica de negocio
     * <p>
     * Este interceptor no realiza envío de eventos o bitácoras
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @return dependiendo de las entradas que reciba el método, estas son devueltas
     * @throws Throwable error
     */
    @Around(value = "pe.com.bn.ms.tracert.SystemArchitecture.inBusinessServiceLayer()")
    public Object loggerAroundBusinessLayer(ProceedingJoinPoint joinPoint) throws Throwable
    {
        return loggerService.aroundLogger(joinPoint, uuid, false);
    }

    /**
     * =================================================================================================================
     * Se realiza el envío de una bitácora, cuando este se cierra correctamente en:
     * - Capa web
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @param result    es el objeto que contiene la respuesta del método una vez este haya finalizado correctamente
     */
    @AfterReturning(pointcut = "pe.com.bn.ms.tracert.SystemArchitecture.inWebLayer()",
                    returning = "result")
    public void loggerAfterReturnRabbitWebLayer(JoinPoint joinPoint, Object result)
    {
        if (valuemqn.equals("true"))
            rabbitService.sendRabbitQueueResponse(joinPoint, result, httpRequest, uuid);
    }

    /**
     * =================================================================================================================
     * Intercepta los errores que ocurren a nivel de:
     * - Capa web
     * - Capa business
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @param throwing  error capturado durante la ejecución de la rutina
     * @throws Throwable error
     */
    @AfterThrowing(pointcut = "pe.com.bn.ms.tracert.SystemArchitecture.inWebLayer() || " +
                              "pe.com.bn.ms.tracert.SystemArchitecture.inBusinessServiceLayer()"
            , throwing = "throwing")
    public void loggerAfterThrowable(JoinPoint joinPoint, Throwable throwing) throws Throwable
    {
        if (valuemqn.equals("true")) {
            loggerService.afterThrowableLogger(joinPoint, uuid, throwing);
            rabbitService.sendRabbitQueueResponseEx(joinPoint, uuid, httpRequest, throwing);
        }
    }

    /**
     * =================================================================================================================
     * Realiza la verificación de la cadena de conexión a base de datos
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @throws Throwable error
     */
//    @Before("pe.com.bn.ms.tracert.SystemArchitecture.validStringConnection()")
//    public void validateSqlConnection(JoinPoint joinPoint) throws Throwable
//    {
//        log.info("============================================================================");
//        log.info("=================== VALIDACIÓN DE PARÁMETROS DE CONEXIÓN ===================");
//        log.info("============================================================================");
//
//        Object     dataSourceObject = joinPoint.getArgs()[0];
//        DataSource hikariDataSource = (DataSource) dataSourceObject;
//
//        try {
//            log.info("Validando Sql getConnection()");
//            hikariDataSource.getConnection();
//            log.info("Validación OK");
//        } catch (Throwable throwing) {
//            log.error("Los parámetros ingresados no son correctos...");
//            uuid = UUID.randomUUID();
//            log.info("Enviando error a logs...");
//            loggerService.afterThrowableLogger(joinPoint, uuid, throwing);
//            log.info("Enviando error a evento...");
//            rabbitService.sendRabbitQueueResponseEx(joinPoint, uuid, httpRequest, throwing);
//            log.info("Cancelando startup...");
//            ctx.close();
//            log.error("Fin de envío de errores...");
//
//            log.info("============================================================================");
//            log.info("==================== ERROR EN LA VALIDACIÓN DE CONEXIÓN ====================");
//            log.info("============================================================================");
//        }
//
//        log.info("============================================================================");
//        log.info("================ FIN DE VALIDACIÓN DE PARÁMETROS DE CONEXIÓN ===============");
//        log.info("============================================================================");
//    }

}
