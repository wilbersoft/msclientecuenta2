package pe.com.bn.ms.tracert.rabbit;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pe.com.bn.ms.tracert.domain.ExceptionFields;
import pe.com.bn.ms.tracert.domain.RabbitPlot;
import pe.com.bn.ms.tracert.domain.RabbitRequest;
import pe.com.bn.ms.tracert.domain.RabbitRequestEx;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static pe.com.bn.ms.util.PayloadValidator.writeValueAsString;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.setterNoSorted;
import static pe.com.bn.ms.util.functions.TraceUtil.extractInputBodyRequest;

/**
 * @author prov_destrada
 * @created 26/10/2020 - 01:01 p. m.
 * @project ms-dataclients
 */
@Log4j2
@Component
@SuppressWarnings("all")
public class RabbitServiceImpl implements RabbitService
{
    @Value("${spring.application.name}") private String applicationName;

    private final RabbitTemplate template;
    private final ObjectMapper   mapper;

    @Autowired
    public RabbitServiceImpl(RabbitTemplate template, ObjectMapper mapper)
    {
        this.template = template;
        this.mapper   = mapper;
    }

    /**
     * =================================================================================================================
     * Envío de bitácoras, las bitácoras son transacciones exitosas
     * LOG_LEVEL: INFO
     *
     * @param joinPoint   contiene todos los objetos usados como parámetros y son enviados a través del método
     * @param result      resultado después de finalizar la operación
     * @param httpRequest
     * @param uuid        uuid
     */
    @Override
    public void sendRabbitQueueResponse(JoinPoint joinPoint, Object result, HttpServletRequest httpRequest, UUID uuid)
    {
        /*log.debug("================================================================");
        log.debug("================= SE ENVÍA BITÁCORA A PILA ELK =================");
        log.debug("================================================================");

        RabbitRequest rabbitRequest = new RabbitRequest();

        String className  = joinPoint.getTarget().getClass().toString();
        String methodName = joinPoint.getSignature().getName().toString();

        // ---------------------------------------------
        // Extraer los elementos INPUT (body, headers, params, etc)
        RabbitPlot rabbitPlot = extractInputBodyRequest(joinPoint);

        // ---------------------------------------------
        // Setear los parámetros en el objeto request
        rabbitRequest.setAplicacion(rabbitPlot.getApplication());
        rabbitRequest.setCanal(rabbitPlot.getChannel());
        rabbitRequest.setIp(rabbitPlot.getIp());

        rabbitRequest.setId(uuid.toString().replace("-", ""));
        rabbitRequest.setCollection(applicationName);

        // Seteo de parámetros INPUT, OUTPUT
        ResponseEntity<?> response = (ResponseEntity<?>) result;

        try {
            rabbitPlot.getData().setOutput(response.getBody());
        } catch (Throwable ex) {
            log.debug("Ocurrio un error mientras se leía el objeto data response: {}", ex.getMessage());
        }

        rabbitPlot.getData().setUrl(httpRequest.getMethod() + " | " + httpRequest.getRequestURI());
        rabbitPlot.getData().setMethod(className + "#" + methodName);

        rabbitRequest.setDatos(rabbitPlot.getData());

        log.debug("Se envía: {}", writeValueAsString(rabbitRequest));
        log.debug("================================================================");
        log.debug("============= FIN DE ENVÍO DE BITÁCORA A PILA ELK ==============");
        log.debug("================================================================");

        template.convertAndSend("exBitacora", "bitacora",
                                writeValueAsString(rabbitRequest));*/
    }

    /**
     * =================================================================================================================
     * Envío de eventos, los eventos son transacciones fallidas
     * LOG_LEVEL: ERROR
     *
     * @param joinPoint   contiene todos los objetos usados como parámetros y son enviados a través del método
     * @param uuid        uuid
     * @param httpRequest
     * @param ex          error capturado
     */
    @Override
    public void sendRabbitQueueResponseEx(JoinPoint joinPoint, UUID uuid, HttpServletRequest httpRequest, Throwable ex)
    throws Throwable
    {
//        log.debug("================================================================");
//        log.debug("================== SE ENVÍA EVENTO A PILA ELK ==================");
//        log.debug("================================================================");
//
//        List<String> methodsFilter = Arrays.asList("aroundLoggerTime", "aroundLogger");
//        String       methodName    = joinPoint.getSignature().getName();
//
//        boolean filter = methodsFilter
//                .stream()
//                .noneMatch(q -> q.equals(methodName));
//
//        // Filtrar los métodos que no deben ser auditados
//        if (filter) {
//
//            RabbitRequestEx rabbitRequestEx = new RabbitRequestEx();
//
//            String className = joinPoint.getTarget().getClass().toString();
//
//            // ---------------------------------------------
//            // Extraer los headers del joinPoint
//            Object[] elements = joinPoint.getArgs();
//
//            RabbitPlot rabbitPlot = extractInputBodyRequest(joinPoint);
//
//            // ---------------------------------------------
//            // Extraer código y mensaje de Throwable
//            String throwName = ex.getClass().getName();
//            throwName = throwName.substring(throwName.lastIndexOf(".") + 1);
//
//            ExceptionFields exceptionFields = new ExceptionFields();
//
//            if (!throwName.matches("Exception|Throwable")) {
//                exceptionFields = setterNoSorted(exceptionFields, ex);
//            }
//
//            // ---------------------------------------------
//            // Setear los parámetros en el objeto request
//            rabbitRequestEx.setIp(rabbitPlot.getIp());
//            rabbitRequestEx.setAplicacion(rabbitPlot.getApplication());
//            rabbitRequestEx.setCanal(rabbitPlot.getChannel());
//
//            rabbitRequestEx.setCodError(exceptionFields.getErrorCode());
//            rabbitRequestEx.setDesError(exceptionFields.getTechnicalMessage());
//            rabbitRequestEx.setLevelError("ERROR");
//            rabbitRequestEx.setSubTipo("");
//            rabbitRequestEx.setPrograma(className + "#" + methodName);
//            rabbitRequestEx.setTipoOperacion(rabbitPlot.getTypeOp());
//            rabbitRequestEx.setIndex(applicationName);
//
//            log.debug("Se envía: {}", writeValueAsString(rabbitRequestEx));
//            log.debug("================================================================");
//            log.debug("============== FIN DE ENVÍO DE EVENTO A PILA ELK ===============");
//            log.debug("================================================================");
//
//            template.convertAndSend("exEvento", "evento",
//                                    writeValueAsString(rabbitRequestEx));
//        }
    }

}
