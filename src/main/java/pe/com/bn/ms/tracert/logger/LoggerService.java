package pe.com.bn.ms.tracert.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.UUID;

public interface LoggerService
{

    Object aroundLoggerTime(ProceedingJoinPoint joinPoint, UUID uuid, boolean response) throws Throwable;

    Object aroundLogger(ProceedingJoinPoint joinPoint, UUID uuid, boolean response) throws Throwable;

    void afterThrowableLogger(JoinPoint joinPoint, UUID uuid, Throwable ex);

}
