package pe.com.bn.ms.exception.base4XX;

import lombok.Getter;
import pe.com.bn.ms.dto.ErrorResponse;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_GATEWAY;

/**
 * @author prov_destrada
 * @description Contiene los elementos base de una excepción
 */
@Getter
public class GatewayException extends RuntimeException
{
    private final String        errorCode;
    private final String        friendlyMessage;
    private final String        technicalMessage;
    private       Object        parameters;
    private       ErrorResponse error;

    public GatewayException()
    {
        this.errorCode        = HANDLE_GATEWAY.getCodResult();
        this.friendlyMessage  = HANDLE_GATEWAY.getMsg();
        this.technicalMessage = HANDLE_GATEWAY.getMsgError();
    }

    public GatewayException(ErrorResponse error)
    {
        this.errorCode        = HANDLE_GATEWAY.getCodResult();
        this.friendlyMessage  = HANDLE_GATEWAY.getMsg();
        this.technicalMessage = HANDLE_GATEWAY.getMsgError();
        this.error            = error;
    }

    public GatewayException(String parameters)
    {
        this.errorCode        = HANDLE_GATEWAY.getCodResult();
        this.friendlyMessage  = HANDLE_GATEWAY.getMsg();
        this.technicalMessage = HANDLE_GATEWAY.getMsgError();
        this.parameters       = parameters;
    }

    public GatewayException(String friendlyMessage, String technicalMessage, Object parameters)
    {
        this.errorCode        = HANDLE_GATEWAY.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = isBlank(technicalMessage) ? friendlyMessage : technicalMessage;
        this.parameters       = parameters;
    }

    public GatewayException(Throwable cause, String friendlyMessage, Object parameters)
    {
        super(cause);
        this.errorCode        = HANDLE_GATEWAY.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = friendlyMessage;
        this.parameters       = parameters;
    }

    public GatewayException(ErrorResponse error, String friendlyMessage, Object parameters)
    {
        this.errorCode        = HANDLE_GATEWAY.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = friendlyMessage;
        this.parameters       = parameters;
    }

}
