package pe.com.bn.ms.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * BAD_REQUEST - 400
 * Este error se produce cuando el plot request va hasta HOST pero el código
 * de resultado es diferente de 00000
 */
@Getter
@NoArgsConstructor
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ResourceHostEx extends RuntimeException
{
    private String codResult;
    private String msg;
    private String msgError;

    public ResourceHostEx(String codResult, String msgError, String msg)
    {
        super(msgError);
        this.codResult = codResult;
        this.msg       = msg;
        this.msgError  = msgError;
    }

    public ResourceHostEx(String msgError, String msg)
    {
        super(msgError);
        this.msg      = msg;
        this.msgError = msgError;
    }

    public ResourceHostEx(String msgError)
    {
        super(msgError);
        this.msgError = msgError;
    }
}
