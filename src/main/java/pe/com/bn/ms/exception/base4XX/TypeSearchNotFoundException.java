package pe.com.bn.ms.exception.base4XX;

import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_TYPE_SEARCH;

/**
 * @author prov_destrada
 * @description Contiene los elementos base de una excepción
 */
@Getter
public class TypeSearchNotFoundException extends RuntimeException
{
    private final String errorCode;
    private final String friendlyMessage;
    private final String technicalMessage;
    private       Object parameters;

    public TypeSearchNotFoundException()
    {
        this.errorCode        = HANDLE_TYPE_SEARCH.getCodResult();
        this.friendlyMessage  = HANDLE_TYPE_SEARCH.getMsg();
        this.technicalMessage = HANDLE_TYPE_SEARCH.getMsgError();
    }

    public TypeSearchNotFoundException(String parameters)
    {
        this.errorCode        = HANDLE_TYPE_SEARCH.getCodResult();
        this.friendlyMessage  = HANDLE_TYPE_SEARCH.getMsg();
        this.technicalMessage = HANDLE_TYPE_SEARCH.getMsgError();
        this.parameters       = parameters;
    }

    public TypeSearchNotFoundException(String friendlyMessage, String technicalMessage, Object parameters)
    {
        this.errorCode        = HANDLE_TYPE_SEARCH.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = isBlank(technicalMessage) ? friendlyMessage : technicalMessage;
        this.parameters       = parameters;
    }

    public TypeSearchNotFoundException(Throwable cause, String friendlyMessage, Object parameters)
    {
        super(cause);
        this.errorCode        = HANDLE_TYPE_SEARCH.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = friendlyMessage;
        this.parameters       = parameters;
    }

}
