package pe.com.bn.ms.exception;

import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_GENERIC;

/**
 * @author prov_destrada
 * @description Contiene los elementos base de una excepción
 */
@Getter
public class GenericException extends RuntimeException
{
    private final String codResult;
    private final String msg;
    private final String msgError;
    private       Object parameters;

    public GenericException()
    {
        this.codResult = HANDLE_GENERIC.getCodResult();
        this.msg       = HANDLE_GENERIC.getMsg();
        this.msgError  = HANDLE_GENERIC.getMsgError();
    }

    public GenericException(String parameters)
    {
        this.codResult  = HANDLE_GENERIC.getCodResult();
        this.msg        = HANDLE_GENERIC.getMsg();
        this.msgError   = HANDLE_GENERIC.getMsgError();
        this.parameters = parameters;
    }

    public GenericException(String msg, String msgError, String parameters)
    {
        this.codResult  = HANDLE_GENERIC.getCodResult();
        this.msg        = msg;
        this.msgError   = isBlank(msgError) ? msg : msgError;
        this.parameters = parameters;
    }

    public GenericException(Throwable cause, String msg, String parameters)
    {
        super(cause);
        this.codResult  = HANDLE_GENERIC.getCodResult();
        this.msg        = msg;
        this.msgError   = msg;
        this.parameters = parameters;
    }

}
