package pe.com.bn.ms.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * REQUEST_TIMEOUT - 408
 * Dispara un error cuando el tiempo de espera y el número de reintentos han sido
 * superados
 */
@Getter
@NoArgsConstructor
@ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
public class ConnectionTimeOutEx extends RuntimeException
{
    private String codResult;
    private String msg;
    private String msgError;

    public ConnectionTimeOutEx(String codResult, String msgError, String msg)
    {
        super(msgError);
        this.codResult = codResult;
        this.msg       = msg;
        this.msgError  = msgError;
    }

    public ConnectionTimeOutEx(String msgError, String msg)
    {
        super(msgError);
        this.msg      = msg;
        this.msgError = msgError;
    }

    public ConnectionTimeOutEx(String msgError)
    {
        super(msgError);
        this.msgError = msgError;
    }
}
