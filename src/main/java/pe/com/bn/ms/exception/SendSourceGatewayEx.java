package pe.com.bn.ms.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import pe.com.bn.ms.dto.GatewayResponse;

/**
 * Este error ocurre, cuando se tiene que pivotear un error que ocurrió
 * en un nivel inferior para mostrarse en uno superior, como otro cliente o servicio.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ResponseStatus(HttpStatus.OK)
public class SendSourceGatewayEx extends RuntimeException
{
    private GatewayResponse genericResponse;
}
