package pe.com.bn.ms.exception.base4XX;

import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_FIELD;

/**
 * @author prov_destrada
 * @description Contiene los elementos base de una excepción
 */
@Getter
public class FieldConstraintException extends RuntimeException
{
    private final String errorCode;
    private final String friendlyMessage;
    private final String technicalMessage;
    private       Object parameters;

    public FieldConstraintException()
    {
        this.errorCode        = HANDLE_FIELD.getCodResult();
        this.friendlyMessage  = HANDLE_FIELD.getMsg();
        this.technicalMessage = HANDLE_FIELD.getMsgError();
    }

    public FieldConstraintException(String parameters)
    {
        this.errorCode        = HANDLE_FIELD.getCodResult();
        this.friendlyMessage  = HANDLE_FIELD.getMsg();
        this.technicalMessage = HANDLE_FIELD.getMsgError();
        this.parameters       = parameters;
    }

    public FieldConstraintException(String technicalMessage, Object parameters)
    {
        this.errorCode        = HANDLE_FIELD.getCodResult();
        this.friendlyMessage  = HANDLE_FIELD.getMsg();;
        this.technicalMessage = isBlank(technicalMessage) ? friendlyMessage : technicalMessage;
        this.parameters       = parameters;
    }

    public FieldConstraintException(String friendlyMessage, String technicalMessage, Object parameters)
    {
        this.errorCode        = HANDLE_FIELD.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = isBlank(technicalMessage) ? friendlyMessage : technicalMessage;
        this.parameters       = parameters;
    }

    public FieldConstraintException(Throwable cause, String friendlyMessage, Object parameters)
    {
        super(cause);
        this.errorCode        = HANDLE_FIELD.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = friendlyMessage;
        this.parameters       = parameters;
    }

}
