package pe.com.bn.ms.exception;

import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_THROWABLE;

/**
 * @author prov_destrada
 * @description Contiene los elementos base de una excepción
 */
@Getter
public class BaseException extends RuntimeException
{
    private final String errorCode;
    private final String friendlyMessage;
    private final String technicalMessage;
    private       Object parameters;

    public BaseException()
    {
        this.errorCode        = HANDLE_THROWABLE.getCodResult();
        this.friendlyMessage  = HANDLE_THROWABLE.getMsg();
        this.technicalMessage = HANDLE_THROWABLE.getMsgError();
    }

    public BaseException(String parameters)
    {
        this.errorCode        = HANDLE_THROWABLE.getCodResult();
        this.friendlyMessage  = HANDLE_THROWABLE.getMsg();
        this.technicalMessage = HANDLE_THROWABLE.getMsgError();
        this.parameters       = parameters;
    }

    public BaseException(String friendlyMessage, String technicalMessage, Object parameters)
    {
        this.errorCode        = HANDLE_THROWABLE.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = isBlank(technicalMessage) ? friendlyMessage : technicalMessage;
        this.parameters       = parameters;
    }

    public BaseException(Throwable cause, String friendlyMessage, Object parameters)
    {
        super(cause);
        this.errorCode        = HANDLE_THROWABLE.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = friendlyMessage;
        this.parameters       = parameters;
    }

}
