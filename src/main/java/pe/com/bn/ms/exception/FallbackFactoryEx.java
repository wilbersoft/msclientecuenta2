package pe.com.bn.ms.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * BAD_REQUEST - 400
 * Disparar un error controlado que se produjo mientras se consultaba
 * un cliente o servicio.
 */
@Getter
@NoArgsConstructor
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FallbackFactoryEx extends RuntimeException
{
    private String codResult;
    private String msg;
    private String msgError;

    public FallbackFactoryEx(String codResult, String msgError, String msg)
    {
        super(msgError);
        this.codResult = codResult;
        this.msg       = msg;
        this.msgError  = msgError;
    }

    public FallbackFactoryEx(String msgError, String msg)
    {
        super(msgError);
        this.msg      = msg;
        this.msgError = msgError;
    }

    public FallbackFactoryEx(String msgError)
    {
        super(msgError);
        this.msgError = msgError;
    }
}
