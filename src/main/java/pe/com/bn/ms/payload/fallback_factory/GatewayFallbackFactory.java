package pe.com.bn.ms.payload.fallback_factory;

import feign.hystrix.FallbackFactory;
import pe.com.bn.ms.payload.GatewayClient;
import pe.com.bn.ms.payload.fallback_factory.fallback.GatewayFallback;

/**
 * @author prov_destrada
 * @created 14/10/2020 - 05:07 p. m.
 * @project MSDataClients
 */
public class GatewayFallbackFactory implements FallbackFactory<GatewayClient>
{
    @Override
    public GatewayClient create(Throwable cause)
    {
        return new GatewayFallback(cause);
    }
}
