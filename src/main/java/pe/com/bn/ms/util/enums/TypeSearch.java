package pe.com.bn.ms.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author prov_destrada
 * @created 14/10/2020 - 04:01 p. m.
 * @project MSDataClients
 */
@Getter
@AllArgsConstructor
public enum TypeSearch
{
    SEARCH_NUMBER_DOCUMENT("TYPENUMDOC", "Búsqueda por tipo y número de documento", "1"),
    SEARCH_CIC("CIC", "Búsqueda por código único de cliente", "2"),
    SEARCH_NUMBER_PRODUCT("TYPENUMPRODUCT", "Búsqueda por tipo y número de producto", "3"),
    SEARCH_NUMBER_CARD("NUM_CARD", "Búsqueda por número de tarjeta", "4"),
    SEARCH_ADDRESS("COD_ADDRESS", "Búsqueda por tipo dirección (cic+correlativo)", "5"),
    SEARCH_NUMBER_DOCUMENT_SIDG("TYPENUMDOC", "Búsqueda por tipo y número de documento", "6"),

    // ----------------------------------------------------
    ;

    private final String type;
    private final String description;
    private final String search;
}
