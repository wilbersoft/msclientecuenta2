package pe.com.bn.ms.util.enums;

import lombok.Getter;

@Getter
public enum Messages
{
    SUCCESSFUL("0000", "Su operación fue realizada con éxito", ""),
    SUCCESSFUL_HOST("00000", "Operación de HOST exitosa", ""),
    SUCCESSFUL_HOST2("0000", "Operación de HOST exitosa", ""),
    // ---------------------------------------------------
    ;
	
    private final String codResult;
    private final String msg;
    private final String msgError;

    Messages(String codResult, String msg, String msgError)
    {
        this.codResult = codResult;
        this.msg       = msg.equals("") ? "Se ha producido un error" : msg;
        this.msgError  = msgError.equals("") ? msg : msgError;
    }
}
