package pe.com.bn.ms.util.functions;

import com.machinezoo.noexception.Exceptions;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import pe.com.bn.ms.domain.FieldProperties;
import pe.com.bn.ms.domain.PlotParameter;
import pe.com.bn.ms.exception.base4XX.FieldConstraintException;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.leftPad;
import static org.apache.commons.lang3.StringUtils.rightPad;
import static org.apache.commons.lang3.reflect.FieldUtils.getAllFields;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_UPDATE;
import static pe.com.bn.ms.util.enums.PlotParameters.TYPE_CHARACTER;
import static pe.com.bn.ms.util.enums.PlotParameters.TYPE_CHARACTER_0;
import static pe.com.bn.ms.util.enums.PlotParameters.TYPE_NUMBER;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.setter;

/**
 * @author prov_destrada
 * @created 21/08/2019 - 05:12 p. m.
 * @project ms-dataclients
 */
@Log4j2
public class PlotUtil
{
    /**
     * =================================================================================================================
     * Convierte un objeto a trama
     *
     * @param plotParameters       es el archivo .csv convertido a mapa
     * @param plotSourceParameters mapa que contiene los campos y valores del objeto descompuesto
     * @param type                 define si el tipo de dato es header o body
     * @return trama
     */
    public static String createPlot(LinkedHashMap<String, PlotParameter> plotParameters,
                                    LinkedHashMap<String, FieldProperties> plotSourceParameters,
                                    String type, String operation) throws Throwable
    {

        Map<Integer, String[]> collect;

        collect = plotParameters.entrySet()
                .stream()
                .filter(q -> q.getValue().getType().contains(type))
                .collect(toMap(x -> x.getValue().getPosition(),
                               x -> new String[]{x.getKey(), discriminateTypeField(plotSourceParameters, x)}));

        collect = collect.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                               (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return collect.values()
                .stream()
                .map(strings -> strings[1])
                .collect(Collectors.joining());
    }

    /**
     * =================================================================================================================
     * Realiza un padding dependiendo del tipo de campo
     *
     * @param fieldValues contiene la lista de los valores por campo
     * @param fields      contiene la lista de los campos
     * @return valor de campo con padding
     */
    private static String discriminateTypeField(Map<String, FieldProperties> fieldValues,
                                                Map.Entry<String, PlotParameter> fields)
    {
        String value;
        //AVargas - Inicio - 20211108
        String temporal;
        //AVargas - Fin - 20211108

        //
        if (fields.getValue().getTypeFieldHost().equals(TYPE_NUMBER.getType())) {
            value = leftPad(fieldValues.get(fields.getValue().getInput()) != null ?
                                    fieldValues.get(fields.getValue().getInput()).getValueField() : " ",
                            fields.getValue().getLongitud(), '0');
        }
        //
        else if (fields.getValue().getTypeFieldHost().equals(TYPE_CHARACTER.getType())) {
        	//AVargas - Inicio - 20211106
        	//Homologamos la cadena para evitar enviar caracteres especiales al host
        	temporal = fieldValues.get(fields.getValue().getInput()) != null ? fieldValues.get(fields.getValue().getInput()).getValueField() : " ";
        	temporal = StringUtils.stripAccents(temporal);
        	temporal = temporal.replace("ñ", "#");
        	temporal = temporal.replace("Ñ", "#");
        	/*
            value = rightPad(fieldValues.get(fields.getValue().getInput()) != null ?
                                     fieldValues.get(fields.getValue().getInput()).getValueField() : " ",
                             fields.getValue().getLongitud(), ' ');
            */
        	value = rightPad(fieldValues.get(fields.getValue().getInput()) != null ?
                    temporal : " ",
                    fields.getValue().getLongitud(), ' ');
        	//AVargas - Inicio - 20211106
        }
        //
        else if (fields.getValue().getTypeFieldHost().equals(TYPE_CHARACTER_0.getType())) {
            value = leftPad(fieldValues.get(fields.getValue().getInput()) != null ?
                                    fieldValues.get(fields.getValue().getInput()).getValueField() : " ",
                            fields.getValue().getLongitud(), '0');
        }
        //
        else {
        	if(fields.getValue().getInput().equals("birthDate")) {
        		System.out.println("Valor del birthDate: ");
        		System.out.println(fields.getValue().getInput().equals("birthDate")?fieldValues.get("birthDate").getValueField():"No tiene data de birthDate");
        	}
        	//AVargas - Inicio - 20211106
        	//Homologamos la cadena para evitar enviar caracteres especiales al host
        	temporal = fieldValues.get(fields.getValue().getInput()) != null ? fieldValues.get(fields.getValue().getInput()).getValueField() : " ";
        	temporal = StringUtils.stripAccents(temporal);
        	temporal = temporal.replace("ñ", "#");
        	temporal = temporal.replace("Ñ", "#");
        	/*
        	value = leftPad(fieldValues.get(fields.getValue().getInput()) != null ?
                                    fieldValues.get(fields.getValue().getInput()).getValueField() : " ",
                            fields.getValue().getLongitud(), ' ');
           */
        	value = leftPad(fieldValues.get(fields.getValue().getInput()) != null ?
                    temporal : " ",
                    fields.getValue().getLongitud(), ' ');
        }

        return value;
    }

    /**
     * =================================================================================================================
     *
     * @param map
     * @param operation
     * @return
     */
    public static List<String> listParametersRequest(Map<String, PlotParameter> map, String operation)
    throws Throwable
    {
        return map.values()
                .stream()
                .map(m -> operation.equals(OPERATION_UPDATE.getValue()) ? m.getOutput() : m.getInput())
                .collect(Collectors.toList());
    }

    /**
     * =================================================================================================================
     * Convierte una originClass cualesquiera a un mapa con sus atributos
     *
     * @param originClass es la clase origen a ser descompuesta
     * @return Map<String, DescriptionFields> con los campos y los valores de estos
     * @throws Exception lanza una excepción
     */
    public static <T, K> LinkedHashMap<String, FieldProperties> convertObjectToPlot(T originClass,
                                                                                    List<String[]> nestedObjects)
    throws Throwable
    {
        Class<?> aClass = originClass.getClass();
        Field[]  fields = getAllFields(aClass);

        LinkedHashMap<String, FieldProperties> sourceObjectMap = new LinkedHashMap<>();

        // Recorre la originClass, y las ingresa en un mapa
        for (Field field : fields) {
            field.setAccessible(true);
            FieldProperties fieldProperties;

            // -------------------------------------------------------------
            // En caso existan objetos anidados
            if (!nestedObjects.isEmpty()) {

                for (String[] strings : nestedObjects) {

                    Field fieldClassNested = aClass.getDeclaredField(strings[1]);
                    fieldClassNested.setAccessible(true);
                    
                    K        nestedObject      = (K) fieldClassNested.get(originClass);
                    Class<?> nestedClass       = nestedObject.getClass();
                    Field[]  nestedClassFields = getAllFields(nestedClass);

                    for (Field nestedClassField : nestedClassFields) {
                        nestedClassField.setAccessible(true);
                        
                        String nestedClassFieldValue = nestedClassField.get(nestedObject) != null ?
                                nestedClassField.get(nestedObject).toString().trim() : "";

                        FieldProperties nestedClassFieldProperties = new FieldProperties();
                        nestedClassFieldProperties.setNameField(nestedClassField.getName());
                        nestedClassFieldProperties.setLengthField(nestedClassFieldValue.length());
                        nestedClassFieldProperties.setTypeField(nestedClass);
                        nestedClassFieldProperties.setValueField(nestedClassFieldValue);

                        sourceObjectMap.put(nestedClassField.getName(), nestedClassFieldProperties);
                    }

                }

            }

            // -------------------------------------------------------------
            // Seteando los valores en el objeto DescriptionFields y
            // eliminar los espacios en blanco
            String fieldValue = field.get(originClass) != null ? field.get(originClass).toString() : "";
            fieldValue = StringUtils.trimToEmpty(fieldValue);

            fieldProperties = new FieldProperties(field.getName(),
                                                  fieldValue.length(),
                                                  field.getType(),
                                                  fieldValue);

            sourceObjectMap.put(field.getName(), fieldProperties);
        }

        return sourceObjectMap;
    }

    /**
     * =================================================================================================================
     * Este método permite convertir una trama a un objeto mediante reflections, en caso el objeto tenga una(s) lista(s)
     * como parte de su estructura se debe tener en cuenta lo siguiente en la estructura del .csv
     *
     * @param plotClass     clase del objeto que va a ser descompuesto con reflections
     * @param mapDtoValues  mapa que contiene cada uno de los valores de los campos de la trama
     * @param listDtoValues en caso la plotClass contenga una(s) lista(s)
     * @return retorna un objeto de plotClass
     * @throws Exception error
     */
    public static <T, K> T mapToObject(T plotClass, LinkedHashMap<String, String[]> mapDtoValues,
                                       LinkedHashMap<String[], LinkedHashMap<Integer, List<Object[]>>> listDtoValues)
    throws Throwable
    {
        Class<?> aClass = plotClass.getClass();
        Field[]  fields = getAllFields(aClass);

        LinkedHashMap<String, String[]> hashMap = new LinkedHashMap<>();

        // Recorre el objeto y lo almacena
        for (Field field : fields) {
            field.setAccessible(true);
            String typeField = field.getType().toString();
            String nameField = field.getName();

            typeField = typeField.substring(typeField.lastIndexOf(".") + 1);

            hashMap.put(nameField, new String[]{typeField, null});
        }

        Map<String, Object[]> stringHashMap = new HashMap<>();

        for (Map.Entry<String, String[]> q : mapDtoValues.entrySet()) {
            //
            if (hashMap.containsKey(q.getValue()[0])) {
                String[] values = hashMap.get(q.getValue()[0]);
                stringHashMap.put(q.getValue()[0], new String[]{values[0], q.getValue()[1]});
            }
        }

        // ----------------------------------------------------------------
        // Paso 02
        // Convertir mapa a objeto lista
        if (!listDtoValues.isEmpty()) {

            listDtoValues.entrySet()
                    .forEach(Exceptions.sneak().consumer(nestedObject -> {

                        String nameClass      = nestedObject.getKey()[0];
                        String nameFieldClass = nestedObject.getKey()[1];
                        String rootPackage    = "pe.com.bn.ms.dto.";

                        Class<?> beanClass = Class.forName(rootPackage + nameClass);

                        K nestedDto = (K) beanClass.getDeclaredConstructor().newInstance();

                        List<K> mm = nestedObject.getValue()
                                .entrySet()
                                .stream()
                                .map(Exceptions.sneak().function(p -> {
                                    // Instanciar la plotClass de tipo lista
                                    return setter(nestedDto, p.getValue());
                                }))
                                .collect(Collectors.toList());

                        BeanUtils.setProperty(plotClass, nameFieldClass, mm);
                    }));

        }

        // Setear los valores del mapa
        for (Map.Entry<String, Object[]> q : stringHashMap.entrySet())
            BeanUtils.setProperty(plotClass, q.getKey(), q.getValue()[1]);

        return plotClass;
    }

    /**
     * =================================================================================================================
     * Validación de campo
     *
     * @param plotParameters
     * @param plotSourceParameters
     * @throws Throwable error
     */
    public static void validateFieldConstraint(LinkedHashMap<String, PlotParameter> plotParameters,
                                               LinkedHashMap<String, FieldProperties> plotSourceParameters)
    throws Throwable
    {
        // Validar los constraints
        if (plotSourceParameters.size() > 0)
            for (FieldProperties propertyField : plotSourceParameters.values()) {
                String fieldValue = propertyField.getValueField();
                String fieldName  = propertyField.getNameField();

                int fieldValueLength = fieldValue.length();
                int length           = propertyField.getLengthField();

                String type = plotParameters.values()
                        .stream()
                        .filter(plotParameter -> plotParameter.getInput().equals(fieldName))
                        .map(PlotParameter::getTypeFieldHost)
                        .collect(toSingleton());

                // Validar la longitud
                if (fieldValueLength > length)
                    throw new FieldConstraintException(
                            "Se ha excedido la longitud permitida de: " + length + " caracteres",
                            fieldName + ": " + fieldValue);

                // Validar tipo numérico solamente
                if (type.equals("N") && (!fieldValue.matches("^[0-9]*$") || isBlank(fieldValue)))
                    throw new FieldConstraintException("El campo solo admite números como valor",
                                                       fieldName + ": " + fieldValue);

                // Validar tipo texto solamente
//                if (type.matches("CN|CN0") && !fieldValue.matches("^(\\w+ ?)[a-zA-Z]*$"))
//                    throw new FieldConstraintException("El campo solo admite caracteres como valor",
//                                                       fieldName + ": " + fieldValue);
            }
        else
            throw new Exception("No existen campos para validar");
    }

    public static <T> Collector<T, ?, T> toSingleton()
    {
        return Collectors.collectingAndThen(
                Collectors.toList(),
                list -> {
                    if (list.size() != 1) {
                        throw new IllegalStateException();
                    }
                    return list.get(0);
                }
        );
    }
}
