package pe.com.bn.ms.util;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CharSequenceReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import pe.com.bn.ms.domain.PlotParameter;

import java.io.InputStream;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author prov_destrada
 * @created 22/10/2020 - 06:14 p. m.
 * @project ms-dataclients
 */
@Log4j2
@Component
public class PlotSource
{
    private final ResourceLoader loader;

    @Autowired
    public PlotSource(ResourceLoader loader) {this.loader = loader;}

    /**
     * =================================================================================================================
     *
     * @param plotClass dto del cual se van a extraer las propiedades desde el .csv
     * @param colophon  indica el colofón del archivo .csv (ejem. '_update')
     * @return {@link Map<String,   PlotParameter  >}
     * @throws Throwable error
     */
    public LinkedHashMap<String, PlotParameter> fillMap(Object plotClass, String colophon) throws Throwable
    {
        CSVParser csvParser = loadFieldPropertiesFromCsvSource(plotClass, colophon);

        LinkedHashMap<String, PlotParameter> hashMap = new LinkedHashMap<>();

        for (CSVRecord csvRecord : csvParser) {
            // Accessing values by Header names
            String POSICION       = csvRecord.get("POSICION");
            String TYPE           = csvRecord.get("TYPE");
            String TYPE_FIELD     = csvRecord.get("TYPE_FIELD");
            String NAME_HOST      = csvRecord.get("NAME_HOST");
            String IN             = csvRecord.get("IN").toUpperCase();
            String INPUT          = csvRecord.get("INPUT");
            String OUT            = csvRecord.get("OUT").toUpperCase();
            String OUTPUT         = csvRecord.get("OUTPUT");
            String LONGITUD       = csvRecord.get("LONGITUD");
            String X1             = csvRecord.get("X1");
            String X2             = csvRecord.get("X2");
            String CHECK_RESPONSE = csvRecord.get("CHECK_RESPONSE").toUpperCase();
            String CHECKPOINT     = csvRecord.get("CHECKPOINT");

            PlotParameter plotParameter =
                    new PlotParameter(POSICION, TYPE, TYPE_FIELD, IN, INPUT,
                                      OUT, OUTPUT, LONGITUD, X1, X2, CHECK_RESPONSE, CHECKPOINT);

            hashMap.put(NAME_HOST, plotParameter);
        }

        return hashMap;
    }

    /**
     * =================================================================================================================
     * Lee los archivos .csv para interpretar las tramas
     *
     * @param plotClass dto del cual se van a extraer las propiedades desde el .csv
     * @param colophon  indica el colofón del archivo .csv (ejem. '_update')
     * @return {@link CSVParser}
     * @throws Throwable error
     */
    public CSVParser loadFieldPropertiesFromCsvSource(Object plotClass, String colophon) throws Throwable
    {
        // --------------------------------------------
        // Leer el .csv según el nombre de la plotClass
        String nameClass = plotClass.getClass().getName();
        nameClass = nameClass.substring(nameClass.lastIndexOf(".") + 1).concat(colophon).concat(".csv");
        String classpath = "classpath:plot/" + nameClass;

        // --------------------------------------------
        // Leer el archivo file
        Resource resource = loader.getResource(classpath);

        InputStream stream       = resource.getInputStream();
        byte[]      buffer       = IOUtils.toByteArray(stream);
        Reader      targetReader = new CharSequenceReader(new String(buffer));
        targetReader.close();

        // --------------------------------------------
        //
        CSVFormat format = CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withIgnoreHeaderCase()
                .withTrim();

        return new CSVParser(targetReader, format);
    }
}
