package pe.com.bn.ms.util.Shared;

import lombok.Data;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data

public class PropertiesParam {
	private String hdLong = "9999";
	private String hdTran = "PC01";
	private String hdTimestamp = getCurrentFormattedDate();
	private String hdCodTran1Format = "612";
	
	public static String getCurrentFormattedDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-999999");
        String formattedDate = dateFormat.format(new Date());
        return formattedDate;
    }
	
	
}
