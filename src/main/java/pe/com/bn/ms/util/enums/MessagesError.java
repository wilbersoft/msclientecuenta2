package pe.com.bn.ms.util.enums;

import lombok.Getter;

@Getter
public enum MessagesError
{
    // ---------------------------------------------------
    // Mensajes de error personalizados

    HANDLE_THROWABLE("9999", "", "Excepción no controlada"),
    HANDLE_EXCEPTION("9999", "", "Excepción no controlada"),

    // 400

    HANDLE_GENERIC("4000", "", "Se produjo un error, lea la descripción del error para mas detalles"),
    HANDLE_HEADER("4001", "", "Excepción de parseo de headers"),
    HANDLE_NOT_EQUAL("4002", "", "Los parámetros del dto son nulos/vacíos"),
    HANDLE_NOT_FOUND("4003", "La búsqueda solicitada no ha producido ningún resultado", ""),
    HANDLE_TYPE_SEARCH("4004", "El tipo de búsqueda no ha producido ningún resultado", ""),
    HANDLE_VALIDATE("4005", "", "Error de validación"),
    HANDLE_BAD_DATA("4006", "El historial del cliente no es correcto", ""),
    HANDLE_GATEWAY("4007", "El código de respuesta del gateway retornó error", ""),
    HANDLE_FIELD("4008", "Error de validación de campo", "Error de validación de campo"),
    HANDLE_HOST("4009", "La transacción de HOST falló", "La transacción de HOST falló"),

    // 500
    HANDLE_CLIENT("5001", "", "Ocurrió un error cuando se consultaba el cliente"),

    // ---------------------------------------------------
    ;
    private final String codResult;
    private final String msg;
    private final String msgError;

    MessagesError(String codResult, String msg, String msgError)
    {
        this.codResult = codResult;
        this.msg       = msg.equals("") ? "Se ha producido un error" : msg;
        this.msgError  = msgError.equals("") ? msg : msgError;
    }
}
