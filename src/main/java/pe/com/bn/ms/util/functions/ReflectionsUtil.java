package pe.com.bn.ms.util.functions;

import com.machinezoo.noexception.Exceptions;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.http.HttpHeaders;
import pe.com.bn.ms.domain.Headers;
import pe.com.bn.ms.exception.base4XX.HostResponseException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static pe.com.bn.ms.util.enums.Messages.SUCCESSFUL_HOST;
import static pe.com.bn.ms.util.enums.Messages.SUCCESSFUL_HOST2;
import static pe.com.bn.ms.util.functions.LetterUtil.capitalizeFirstLetter;


/**
 * @author prov_destrada
 * @created 26/10/2020 - 01:01 p. m.
 * @project ms-dataclients
 */
@Log4j2
@SuppressWarnings("all")
public class ReflectionsUtil
{
    /**
     * =================================================================================================================
     * Este método permite acceder a todos los recursos extendidos de una clase
     *
     * @param type tipo de clase
     * @return lista de campos del objeto
     */
    private static Field[] getAllFields(Class<?> type)
    {
        List<Field> fields = new ArrayList<>();
        for (Class<?> c = type; c != null; c = c.getSuperclass()) {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }

        return fields.toArray(new Field[0]);
    }

    /**
     * =================================================================================================================
     * Inserta valor a los campos de un DTO, el orden de de los valores campos debe coincidir con el orden que tienen en
     * el DTO al cual van a registrarse
     *
     * @param aClass    contiene todos los campos de un DTO
     * @param dtoValues contiene todos los valores de los campos de un DTO
     * @return {@link T}
     */
    public static <T> T setter(Class<?> aClass, Object[] dtoValues) throws Throwable
    {
        Field[] fields = getAllFields(aClass);

        T t = (T) aClass.getDeclaredConstructor().newInstance();

        // --------------------------------------------
        // Recorrer cada uno de los valores
        int counter = 0;
        for (Field field : fields) {
            field.setAccessible(true);
            String fieldName = field.getName();

            BeanUtils.setProperty(t, fieldName, dtoValues[counter]);
            counter++;
        }

        return t;
    }

    /**
     * =================================================================================================================
     * Inserta valor a los campos de una lista DTO, el orden de de los valores campos debe coincidir con el orden que
     * tienen en la lista DTO al cual van a registrarse
     *
     * @param aClass    contiene todos los campos de un DTO
     * @param allFields contiene todos los nombres y valores de los campos de una lista DTO
     * @return {@link List<T>}
     */
    public static <T> List<T> setter(Class<?> aClass, List<Object[]> allFields) throws Throwable
    {
        Field[] fields = getAllFields(aClass);

        List<T> multipleDto = new ArrayList<>();

        for (Object[] singleDto : allFields) {
            T t = (T) aClass.getDeclaredConstructor().newInstance();

            // --------------------------------------------
            // Recorrer cada uno de los valores
            int counter = 0;
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName();

                BeanUtils.setProperty(t, fieldName, singleDto[counter]);
                counter++;
            }
            multipleDto.add(t);
        }

        return multipleDto;
    }

    /**
     * =================================================================================================================
     * Inserta valor a los campos de una lista DTO, el orden no interesa pues realizará una búsqueda por coincidencia
     * en los campos
     *
     * @param allFields objeto donde se setearan los valores
     * @param allValues objeto desde el cual se obtendran los valores para setear
     * @return {@link List<T>}
     */
    public static <T, K> T setterNoSorted(T allFields, K allValues) throws Throwable
    {
        Class<?> tClass  = allFields.getClass();
        Field[]  tFields = getAllFields(tClass);

        Class<?> kClass  = allValues.getClass();
        Field[]  kFields = getAllFields(kClass);

        T t = (T) tClass.getDeclaredConstructor().newInstance();

        List<String> tNameFields = Arrays.stream(tFields)
                .map(m -> m.getName())
                .collect(Collectors.toList());

        List<String> kNameFields = Arrays.stream(kFields)
                .map(m -> m.getName())
                .collect(Collectors.toList());

        tNameFields.stream()
                .filter(f -> kNameFields.contains(f))
                .forEach(Exceptions.sneak().consumer(f -> {
                    Field kField = allValues.getClass().getDeclaredField(f);
                    kField.setAccessible(true);
                    Object value = kField.get(allValues);

                    if (value != null) {
                        BeanUtils.setProperty(t, f, kField.get(allValues));
                    }
                }));

        return t;
    }

    /**
     * =================================================================================================================
     * Inserta valor a los campos de una lista DTO, el orden de de los valores campos debe coincidir con el orden que
     * tienen en la lista DTO al cual van a registrarse
     *
     * @param aClass            contiene todos los campos de un DTO
     * @param multipleDtoValues contiene todos los valores de los campos de una lista DTO
     * @return {@link List<T>}
     */
    public static <T> T setter(T dto, List<Object[]> multipleDtoValues) throws Throwable
    {
        Class<?> aClass = dto.getClass();
        Field[]  fields = getAllFields(aClass);

        T t = (T) aClass.getDeclaredConstructor().newInstance();

        for (Object[] singleDto : multipleDtoValues) {

            String nameField  = singleDto[0].toString();
            Object valueField = singleDto[1];

            BeanUtils.setProperty(t, nameField, valueField);
        }

        return t;
    }

    /**
     * =================================================================================================================
     * Convertir un {@link HttpHeaders} a {@link Headers}
     *
     * @param httpHeaders {@link HeaderDTO}
     * @return {@link HttpHeaders}
     */
    public static Headers httpHeadersToHeaders(HttpHeaders httpHeaders) throws Throwable
    {
        Headers headers = new Headers();

        Class<?> aClass = headers.getClass();
        Field[]  fields = getAllFields(aClass);

        // Recorrer cada uno de los elementos de dto Headers
        // Almacenar si existe en HttpHeaders
        for (Field field : fields) {
            field.setAccessible(true);
            String fieldName           = field.getName();
            String fieldNameCapitalize = capitalizeFirstLetter(fieldName);

            if (httpHeaders.containsKey(fieldNameCapitalize)) {
                String header = httpHeaders.getFirst(fieldNameCapitalize);
                BeanUtils.setProperty(headers, fieldName, header);
            }

        }

        return headers;
    }

    /**
     * =================================================================================================================
     * Validar que la trama de respues sea OK
     *
     * @param responseEntity {@link ResponseEntity<GatewayResponse>}
     * @return {@link GatewayResponse}
     * @throws Throwable error
     */
    public static <T> void validatePlotResponse(T response) throws Throwable
    {
        Class<?> tClass = response.getClass();
        Field[]  fields = getAllFields(tClass);

        // --------------------------------------------
        // Recorrer cada uno de los valores
        String codeResult = "";
        String msgResult  = "";

        for (Field field : fields) {
            field.setAccessible(true);
            String fieldName = field.getName();

            if (fieldName.equals("hdCodeHostResult"))
                codeResult = (String) field.get(response);
            if (fieldName.equals("hdMessageHostResult"))
                msgResult = (String) field.get(response);
        }

        // Validar la respuesta de HOST
        if (!codeResult.equals(SUCCESSFUL_HOST.getCodResult()))
            throw new HostResponseException("CodeResult:" + codeResult + ", Message: " + msgResult);
        else
            log.info("Transacción de HOST exitosa");
    }
    

    /**
     * =================================================================================================================
     * Validar que la trama de respues sea OK
     *
     * @param responseEntity {@link ResponseEntity<GatewayResponse>}
     * @return {@link GatewayResponse}
     * @throws Throwable error
     */
    public static <T> void validatePlotResponse2(T response) throws Throwable
    {
        Class<?> tClass = response.getClass();
        Field[]  fields = getAllFields(tClass);

        // --------------------------------------------
        // Recorrer cada uno de los valores
        String codeResult = "";
        String msgResult  = "";

        for (Field field : fields) {
            field.setAccessible(true);
            String fieldName = field.getName();

            if (fieldName.equals("hdCodeHostResult"))
                codeResult = (String) field.get(response);
            if (fieldName.equals("hdMessageHostResult"))
                msgResult = (String) field.get(response);
        }

        // Validar la respuesta de HOST
        if (!codeResult.equals(SUCCESSFUL_HOST2.getCodResult()))
            throw new HostResponseException("CodeResult:" + codeResult + ", Message: " + msgResult);
        else
            log.info("Transacción de HOST exitosa");
    }

}
