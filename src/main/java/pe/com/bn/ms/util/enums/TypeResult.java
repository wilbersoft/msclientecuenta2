package pe.com.bn.ms.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author prov_destrada
 * @created 14/10/2020 - 04:01 p. m.
 * @project MSDataClients
 */
@Getter
@AllArgsConstructor
public enum TypeResult
{
    RESULT_CUSTOMER("", "Resultado cliente", "1"),
    RESULT_PRODUCT("", "Resultado producto", "2"),
    RESULT_ADDRESS("", "Resultado dirección", "3"),
    RESULT_CARD("", "Resultado tarjeta", "4"),
    RESULT_INDICATORS("", "Resultado indicadores cliente", "5")

    // ----------------------------------------------------
    ;

    private final String type;
    private final String description;
    private final String result;
}
