package pe.com.bn.ms.config.feign;

import feign.Logger;
import feign.Request;
import feign.Retryer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * Configuracion de Feign para servicios en red LAN
 */
@Slf4j
public class FeignWanConfig
{
    @Bean
    public Logger.Level configureLogLevel()
    {
        return Logger.Level.HEADERS;
    }

    @Bean
    public Request.Options timeoutConfiguration()
    {
        // Default 10 seconds
        return new Request.Options();
    }

    @Bean
    public Retryer retryer()
    {
        return new Retryer.Default(2L, 300L, 4);
    }
}
