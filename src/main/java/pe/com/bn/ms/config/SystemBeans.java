package pe.com.bn.ms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SystemBeans
{
    @Bean
    public RestTemplate retrieveRestTemplate()
    {
        return new RestTemplate();
    }

//    @Bean
//    public ServiceReniec2Service retrieveConsultaReniec()
//    {
//        return new ServiceReniec2Service();
//    }

}
