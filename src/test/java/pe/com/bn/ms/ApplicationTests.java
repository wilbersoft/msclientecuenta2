package pe.com.bn.ms;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.bn.ms.domain.TypeSearchCustomer;
import pe.com.bn.ms.domain.TypeSearchProduct;

@SpringBootTest
@AutoConfigureMockMvc

class ApplicationTests{
	
	@Autowired
	  private MockMvc mockMvc;

	  @Autowired
	  private ObjectMapper objectMapper;

	  
	@Test
	void ClientSearchWorksThroughAllLayers() throws Exception {
		//Configurar los datos necesarios para la prueba
	    String typeSearch = "CIC";
	    String userApplication = "myUser";
	    String cic = "123456";
	    String typeDoc = "A";
	    String numDoc = "78901234";
	    String typeProduct = "C";
	    String numProduct = "1234567890123456";
	    String flagEntidadExterna = "D";
	    
	    // Crear una instancia de TypeSearchCustomer con los datos a verificar
	    TypeSearchCustomer client = new TypeSearchCustomer(typeSearch, cic, typeDoc, numDoc, typeProduct, numProduct, flagEntidadExterna, userApplication);

	    //Realizar la solicitud HTTP y realizar las comprobaciones correspondientes
	    mockMvc.perform(get("/msdataclients/clients/client/v1/search/{typeSearch}", typeSearch)
	            .param("userApplication", userApplication)
	            .param("cic", cic)
	            .param("typeDoc", typeDoc)
	            .param("numDoc", numDoc)
	            .param("typeProduct", typeProduct)
	            .param("numProduct", numProduct)
	            .param("flagEntidadExterna", flagEntidadExterna)
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(objectMapper.writeValueAsString(client)))
	            .andExpect(status().isOk());
	}

	@Test
	void ProductSearchWorksThroughAllLayers() throws Exception {
		String typeSearch = "CIC";
	    String userApplication = "myUser";
	    String cic = "123456";
	    String typeDoc = "A";
	    String numDoc = "78901234";
	    String typeProduct = "C";
	    String numProduct = "1234567890123456";
	    
	    // Crear una instancia de TypeSearchProduct con los datos a verificar
	    TypeSearchProduct product = new TypeSearchProduct(typeSearch, cic, typeDoc, numDoc, typeProduct, numProduct, userApplication);

	    mockMvc.perform(get("/msdataclients/product/clients/product/v1/search/{typeSearch}", typeSearch)
	            .param("userApplication", userApplication)
	            .param("cic", cic)
	            .param("typeDoc", typeDoc)
	            .param("numDoc", numDoc)
	            .param("typeProduct", typeProduct)
	            .param("numProduct", numProduct)
	            .contentType(MediaType.APPLICATION_JSON)
	    		.content(objectMapper.writeValueAsString(product)))
	            .andExpect(status().isOk());
	}
}

